/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.api;

import cn.easyplatform.dos.EnvDo;
import cn.easyplatform.entities.beans.project.DeviceMapBean;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.messages.vos.EnvVo;
import cn.easyplatform.services.IProjectService;
import cn.easyplatform.type.DeviceType;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.util.MessageUtils;
import cn.easyplatform.util.WxUtil;
import org.apache.shiro.SecurityUtils;

import java.io.File;
import java.util.Map;

/**
 * 小程序快速登陆
 *
 * @Author: <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @Description:
 * @Since: 2.0.0 <br/>
 * @Date: Created in 2020/06/18 13:25
 * @Modified By:
 */
public class QuickCmd extends AbstractCommand<SimpleRequestMessage> {
    /**
     * @param req
     */
    public QuickCmd(SimpleRequestMessage req) {
        super(req);
    }

    @Override
    public IResponseMessage<?> execute(CommandContext cc) {
        Map<String, String> data = (Map<String, String>) req.getBody();
        String app = data.get("app");
        IProjectService ps = cc.getProjectService(app);
        if (ps == null)
            return MessageUtils.projectNotFound();
        EnvVo env = new EnvVo();
        env.setAppContext(app);
        env.setProjectId(ps.getId());
        env.setDeviceType(data.get("device"));
        env.setName(ps.getName());
        env.setLanguages(ps.getLanguages());
        env.setPortlet(data.get("portlet"));;
        DeviceMapBean dm = ps.getDeviceMap(env.getPortlet(), env.getDeviceType());
        if (dm == null)
            return MessageUtils.projectDeviceInvalid();
        if (Strings.isBlank(dm.getTheme()))
            env.setTheme(ps.getEntity().getTheme());
        else
            env.setTheme(dm.getTheme());
        env.setLoginPage(dm.getLoginPage());
        env.setTitle(ps.getDescription());
        env.setSessionId(SecurityUtils.getSubject().getSession().getId());
        env.setMainPage(dm.getMainPage());
        cc.setupEnv(new EnvDo(ps.getId(), DeviceType
                .getType(env.getDeviceType()), ps.getLanguages().get(0),
                env.getPortlet(), env.getVariables()));

        IResponseMessage<?> resp = WxUtil.quick(cc, ps, data.get("type"), data.get("code"), data.get("ip"));
        if (!resp.isSuccess())
            return resp;
        return new SimpleResponseMessage(new Object[]{env, resp.getBody()});
    }
}
