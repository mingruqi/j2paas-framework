/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.service;

import cn.easyplatform.EngineService;
import cn.easyplatform.engine.cmd.list.*;
import cn.easyplatform.messages.request.*;
import cn.easyplatform.spi.service.ListService;
import cn.easyplatform.type.IResponseMessage;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ListServiceImpl extends EngineService implements
        ListService {

    @Override
    public IResponseMessage<?> doInit(ListInitRequestMessage req) {
        return commandExecutor.execute(new InitCmd(req));
    }

    @Override
    public IResponseMessage<?> doPaging(ListPagingRequestMessage req) {
        return commandExecutor.execute(new PagingCmd(req));
    }

    @Override
    public IResponseMessage<?> doQuery(ListQueryRequestMessage req) {
        return commandExecutor.execute(new QueryCmd(req));
    }

    @Override
    public IResponseMessage<?> doRefresh(ListRefreshRequestMessage req) {
        return commandExecutor.execute(new RefreshCmd(req));
    }

    @Override
    public IResponseMessage<?> doSave(ListSaveRequestMessage req) {
        return commandExecutor.execute(new SaveCmd(req));
    }

    @Override
    public IResponseMessage<?> doDelete(ListDeleteRequestMessage req) {
        return commandExecutor.execute(new DeleteCmd(req));
    }

    @Override
    public IResponseMessage<?> doCreate(ListCreateRequestMessage req) {
        return commandExecutor.execute(new CreateCmd(req));
    }

    @Override
    public IResponseMessage<?> doSelect(ListSelectRequestMessage req) {
        return commandExecutor.execute(new SelectCmd(req));
    }

    @Override
    public IResponseMessage<?> doClose(SimpleRequestMessage req) {
        return commandExecutor.execute(new CloseCmd(req));
    }

    @Override
    public IResponseMessage<?> doOpen(ListOpenRequestMessage req) {
        return commandExecutor.execute(new OpenCmd(req));
    }

    @Override
    public IResponseMessage<?> doReload(ListReloadRequestMessage req) {
        return commandExecutor.execute(new ReloadCmd(req));
    }

    @Override
    public IResponseMessage<?> doSort(ListSortRequestMessage req) {
        return commandExecutor.execute(new SortCmd(req));
    }

    @Override
    public IResponseMessage<?> doSelection(ListSelectionRequestMessage req) {
        return commandExecutor.execute(new SelectionCmd(req));
    }

    public IResponseMessage<?> doClear(SimpleTextRequestMessage req) {
        return commandExecutor.execute(new ClearCmd(req));
    }

    @Override
    public IResponseMessage<?> doReset(ListResetRequestMessage req) {
        return commandExecutor.execute(new ResetCmd(req));
    }

    @Override
    public IResponseMessage<?> doDrop(DropRequestMessage req) {
        return commandExecutor.execute(new DropCmd(req));
    }

    @Override
    public IResponseMessage<?> evalList(ListEvalRequestMessage req) {
        return commandExecutor.execute(new EvalCmd(req));
    }

    @Override
    public IResponseMessage<?> setPageSize(ListPagingRequestMessage req) {
        return commandExecutor.execute(new SetPageSizeCmd(req));
    }

    @Override
    public IResponseMessage<?> doLoad(SimpleRequestMessage req) {
        return commandExecutor.execute(new LoadCmd(req));
    }
}
