/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.component;

import cn.easyplatform.dao.BizDao;
import cn.easyplatform.dos.FieldDo;
import cn.easyplatform.entities.beans.list.Header;
import cn.easyplatform.entities.beans.list.ListBean;
import cn.easyplatform.entities.beans.table.TableBean;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.request.GetComboRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.messages.vos.ComboVo;
import cn.easyplatform.type.EntityType;
import cn.easyplatform.type.FieldType;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.util.MessageUtils;
import cn.easyplatform.util.RuntimeUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GetComboCmd extends AbstractCommand<GetComboRequestMessage> {

	/**
	 * @param req
	 */
	public GetComboCmd(GetComboRequestMessage req) {
		super(req);
	}

	@Override
	public IResponseMessage<?> execute(CommandContext cc) {
		ComboVo cm = req.getBody();
		ListBean lb = cc.getEntity(cm.getListId());
		if (lb == null)
			return MessageUtils.entityNotFound(EntityType.DATALIST.getName(),
					cm.getListId());
		List<FieldDo> params = new ArrayList<FieldDo>(cm.getParams().length);
		FieldType type = FieldType.VARCHAR;
		for (Header header : lb.getHeaders()) {
			if (cm.getName().equals(header.getField())) {
				type = header.getType();
				break;
			}
		}
		for (int i = 0; i < cm.getParams().length; i++) {
			FieldDo param = new FieldDo(type);
			param.setName(cm.getName());
			if (type != FieldType.VARCHAR)
				param.setValue(RuntimeUtils.castTo(param, cm.getParams()[i]));
			else
				param.setValue(cm.getParams()[i]);
			params.add(param);
		}
		BizDao dao = null;
		if (!Strings.isBlank(lb.getTable())) {
			TableBean tb = cc.getEntity(lb.getTable());
			if (tb == null)
				return MessageUtils.entityNotFound(EntityType.TABLE.getName(),
						lb.getTable());
			dao = cc.getBizDao(tb.getSubType());
		} else
			dao = cc.getBizDao();
		List<FieldDo[]> data = dao.selectList(cm.getQuery(), params);
		List<String> result = new ArrayList<String>(data.size());
		for (FieldDo[] objs : data)
			result.add((String) objs[0].getValue());
		return new SimpleResponseMessage(result);
	}
}
