/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.demo.table.expert;

import cn.easyplatform.entities.beans.table.TableField;
import cn.easyplatform.type.FieldType;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class MySqlTableExpert extends AbstractTableExpert {

	@Override
	protected String evalFieldType(TableField mf) {
		if (mf.getType() == FieldType.INT) {
			int width = mf.getLength();
			if (width <= 0) {
				return "INT(32)";
			} else if (width <= 2) {
				return "TINYINT(" + (width * 4) + ")";
			} else if (width <= 4) {
				return "MEDIUMINT(" + (width * 4) + ")";
			} else if (width <= 8) {
				return "INT(" + (width * 4) + ")";
			}
			return "BIGINT(" + (width * 4) + ")";
		} else if (mf.getType() == FieldType.LONG)
			return "BIGINT";
		
		if (mf.getType() == FieldType.BLOB) {
			return "MediumBlob"; // 默认用16M
		}
		return super.evalFieldType(mf);
	}
}
