/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.spi.service;

import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.request.SimpleTextRequestMessage;
import cn.easyplatform.type.IResponseMessage;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface AddonService {

    /**
     * 取指定的一个值
     * @param req
     * @return
     */
    IResponseMessage<?> selectObject(SimpleRequestMessage req);

    /**
     * 取多笔记录
     * @param req
     * @return
     */
    IResponseMessage<?> selectList(SimpleRequestMessage req);

    /**
     * 取一笔记录
     * @param req
     * @return
     */
    IResponseMessage<?> selectOne(SimpleRequestMessage req);

    /**
     * 执行sql语句
     * @param req
     * @return
     */
    IResponseMessage<?> update(SimpleRequestMessage req);

    /**
     * @param req
     * @return
     */
    IResponseMessage<?> commit(SimpleRequestMessage req);

    /**
     * 查找参数表
     *
     * @param req
     * @return
     */
    IResponseMessage<?> searchEntity(SimpleRequestMessage req);

    /**
     * @param req
     * @return
     */
    IResponseMessage<?> getEntity(SimpleTextRequestMessage req);

    /**
     * 获取栏位或变量值
     * @param req
     * @return
     */
    IResponseMessage<?> getVariable(SimpleTextRequestMessage req);

    /**
     * 获取即期价格
     *
     * @param req
     * @return
     */
    IResponseMessage<?> getSpot(SimpleRequestMessage req);

    /**
     * 获取远期价格
     *
     * @param req
     * @return
     */
    IResponseMessage<?> getForward(SimpleRequestMessage req);

    /**
     *
     *
     * @param req
     * @return
     */
    IResponseMessage<?> callback(SimpleRequestMessage req);
}
