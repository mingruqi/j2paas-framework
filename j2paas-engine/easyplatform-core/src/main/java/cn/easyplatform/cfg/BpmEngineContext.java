/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.cfg;

import cn.easyplatform.contexts.RecordContext;
import cn.easyplatform.messages.vos.BpmTaskVo;

import java.util.Map;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface BpmEngineContext {

    /**
     * 获取流程开始的功能id
     *
     * @param processId
     */
    String getInstanceTask(String processId);

    /**
     * 启动流程实例并执行
     *
     * @param processId
     * @param ctx
     */
    void startAndExecute(String processId, RecordContext ctx);

    /**
     * 启动流程实例并执行指定的任务
     *
     * @param processId
     * @param nodeName
     * @param ctx
     */
    void startAndExecute(String processId, String nodeName, RecordContext ctx);

    /**
     * 执行指定任务
     *
     * @param taskId
     * @param ctx
     */
    void execute(String taskId, RecordContext ctx);

    /**
     * 根据实例id、创建人、抄送人创建抄送记录
     *
     * @param orderId
     * @param actorIds
     */
    void createCCOrder(String orderId, String... actorIds);

    /**
     * 更新状态用于更新抄送记录为已经阅读
     *
     * @param orderId
     * @param actorIds
     */
    void updateCCStatus(String orderId, String... actorIds);

    /**
     * 任务提取,一般发生在参与者为部门、角色等组的情况下，该组的某位成员提取任务后，不允许其它成员处理该任务
     *
     * @param taskId
     * @param operator
     */
    void take(String taskId, String operator);

    /**
     * 撤回指定的任务
     *
     * @param taskId 任务主键ID
     */
    void withdraw(String taskId, String operator);

    /**
     * 结束当前任务，跳转到指定的nodeName
     *
     * @param taskId
     * @param rc
     * @param nodeName
     */
    void executeAndJumpTask(String taskId, String nodeName, RecordContext rc);

    /**
     * 拒绝
     *
     * @param taskId
     */
    void reject(String taskId, RecordContext rc);

    /**
     * @param taskId
     * @param rc
     * @param actorIds
     */
    void resignTo(String taskId, RecordContext rc, String... actorIds);

    /**
     * 指派给其它人处理
     *
     * @param taskId
     * @param rc
     * @param actorIds
     */
    void resignTo(String taskId, int performType, RecordContext rc,
                  String... actorIds);

    /**
     * 对指定的任务分配参与者。参与者可以为用户、部门、角色
     *
     * @param taskId
     * @param actorIds
     */
    void addTaskActor(String taskId, String... actorIds);

    /**
     * 对指定的任务分配参与者。参与者可以为用户、部门、角色
     *
     * @param taskId
     * @param actorIds
     */
    void addTaskActor(String taskId, int performType, String... actorIds);

    /**
     * 删除其中的参与者
     *
     * @param taskId
     * @param actorIds
     */
    void removeTaskActor(String taskId, String... actorIds);

    /**
     * 终止流程
     *
     * @param orderId
     * @param operator
     */
    void terminate(String orderId, String operator);

    /**
     * 获取流程所有的TaskNode，返回功能id、名称
     *
     * @return
     */
    String[][] getNodes(String processId);

    /**
     * 获取流程中某一个任务结点下一步所有的TaskNode，返回功能id、名称
     *
     * @return
     */
    String[][] getNextNodes(String processId, String nodeName, Map<String, Object> args);

    /**
     * 获取流程中任务结点的参与者，返回参与人、角色、类型
     *
     * @return
     */
    String[] getActors(String processId, String nodeName);

    /**
     * @param id
     * @return
     */
    String[] getView(String id, boolean isProcessId);

    /**
     * @param orderId
     * @param taskName
     * @return
     */
    BpmTaskVo getTask(String orderId, String taskName);
}
