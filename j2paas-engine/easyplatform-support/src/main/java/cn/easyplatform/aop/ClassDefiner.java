/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.aop;

/**
 * 根据字节码，定义一个 Class 文件
 * <p>
 * 它就是一种 ClassLoader，如果已经定义过的 Class，它将不再重复定义
 * 
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a>
 */
public interface ClassDefiner {

	/**
	 * 根据字节码，定义一个 Class 文件，如果已经定义过的 Class，它将不再重复定义
	 * 
	 * @param className
	 *            一个类的全名
	 * @param bytes
	 *            字节码
	 * @return 类对象
	 * @throws ClassFormatError
	 *             字节码格式错误
	 */
	Class<?> define(String className, byte[] bytes) throws ClassFormatError;

	/**
	 * @param className
	 *            一个类全名
	 * @return 是否在缓存中存在这个类的定义
	 */
	boolean has(String className);

	/**
	 * @param className
	 *            一个类的全名
	 * @return 缓存中的类定义
	 * @throws ClassNotFoundException
	 *             如果缓存中没有这个类定义
	 */
	Class<?> load(String className) throws ClassNotFoundException;
}
