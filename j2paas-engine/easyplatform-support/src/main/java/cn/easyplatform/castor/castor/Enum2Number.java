/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.castor.castor;

import cn.easyplatform.castor.Castor;
import cn.easyplatform.castor.FailToCastObjectException;
import cn.easyplatform.lang.Mirror;

@SuppressWarnings({ "rawtypes" })
public class Enum2Number extends Castor<Enum, Number> {

	@Override
	public Number cast(Enum src, Class<?> toType, String... args)
			throws FailToCastObjectException {
		Mirror<?> mi = Mirror.me(src);

		// 首先尝试调用枚举对象的 value() 方法
		try {
			return (Number) mi.invoke(src, "value");
		}
		// 如果失败，就用其顺序号
		catch (Exception e) {
			Integer re = src.ordinal();
			return (Number) Mirror.me(toType).born(re.toString());
		}
	}

}
