/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.jms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.IllegalStateException;
import javax.jms.*;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class PooledConnection implements TopicConnection, QueueConnection, PooledSessionEventListener {
    private static final transient Logger LOG = LoggerFactory.getLogger(PooledConnection.class);

    protected ConnectionPool pool;
    private volatile boolean stopped;
    private final List<TemporaryQueue> connTempQueues = new CopyOnWriteArrayList<TemporaryQueue>();
    private final List<TemporaryTopic> connTempTopics = new CopyOnWriteArrayList<TemporaryTopic>();
    private final List<PooledSession> loanedSessions = new CopyOnWriteArrayList<PooledSession>();

    /**
     * @param pool
     */
    public PooledConnection(ConnectionPool pool) {
        this.pool = pool;
    }

    /**
     * 创建连接池
     */
    public PooledConnection newInstance() {
        return new PooledConnection(pool);
    }

    @Override
    public void close() throws JMSException {
        this.cleanupConnectionTemporaryDestinations();
        this.cleanupAllLoanedSessions();
        if (this.pool != null) {
            this.pool.decrementReferenceCount();
            this.pool = null;
        }
    }

    @Override
    public void start() throws JMSException {
        assertNotClosed();
        pool.start();
    }

    @Override
    public void stop() throws JMSException {
        stopped = true;
    }

    @Override
    public ConnectionConsumer createConnectionConsumer(Destination destination, String selector, ServerSessionPool serverSessionPool, int maxMessages) throws JMSException {
        return getConnection().createConnectionConsumer(destination, selector, serverSessionPool, maxMessages);
    }

    @Override
    public ConnectionConsumer createConnectionConsumer(Topic topic, String s, ServerSessionPool serverSessionPool, int maxMessages) throws JMSException {
        return getConnection().createConnectionConsumer(topic, s, serverSessionPool, maxMessages);
    }

    @Override
    public ConnectionConsumer createDurableConnectionConsumer(Topic topic, String selector, String s1, ServerSessionPool serverSessionPool, int i) throws JMSException {
        return getConnection().createDurableConnectionConsumer(topic, selector, s1, serverSessionPool, i);
    }

    @Override
    public String getClientID() throws JMSException {
        return getConnection().getClientID();
    }

    @Override
    public ExceptionListener getExceptionListener() throws JMSException {
        return pool.getParentExceptionListener();
    }

    @Override
    public ConnectionMetaData getMetaData() throws JMSException {
        return getConnection().getMetaData();
    }

    @Override
    public void setExceptionListener(ExceptionListener exceptionListener) throws JMSException {
        pool.setParentExceptionListener(exceptionListener);
    }

    @Override
    public void setClientID(String clientID) throws JMSException {
        if (this.getConnection().getClientID() == null || !this.getClientID().equals(clientID)) {
            getConnection().setClientID(clientID);
        }
    }

    @Override
    public ConnectionConsumer createConnectionConsumer(Queue queue, String selector, ServerSessionPool serverSessionPool, int maxMessages) throws JMSException {
        return getConnection().createConnectionConsumer(queue, selector, serverSessionPool, maxMessages);
    }

    @Override
    public QueueSession createQueueSession(boolean transacted, int ackMode) throws JMSException {
        return (QueueSession) createSession(transacted, ackMode);
    }

    @Override
    public TopicSession createTopicSession(boolean transacted, int ackMode) throws JMSException {
        return (TopicSession) createSession(transacted, ackMode);
    }

    @Override
    public Session createSession(boolean transacted, int ackMode) throws JMSException {
        PooledSession result = (PooledSession) pool.createSession(transacted, ackMode);
        loanedSessions.add(result);
        result.addSessionEventListener(this);
        return result;
    }

    @Override
    public void onTemporaryQueueCreate(TemporaryQueue tempQueue) {
        connTempQueues.add(tempQueue);
    }

    @Override
    public void onTemporaryTopicCreate(TemporaryTopic tempTopic) {
        connTempTopics.add(tempTopic);
    }

    @Override
    public void onSessionClosed(PooledSession session) {
        if (session != null) {
            this.loanedSessions.remove(session);
        }
    }

    public Connection getConnection() throws JMSException {
        assertNotClosed();
        return pool.getConnection();
    }

    protected void assertNotClosed() throws IllegalStateException {
        if (stopped || pool == null) {
            throw new IllegalStateException("Connection closed");
        }
    }

    protected Session createSession(SessionKey key) throws JMSException {
        return getConnection().createSession(key.isTransacted(), key.getAckMode());
    }

    @Override
    public String toString() {
        return "PooledConnection { " + pool + " }";
    }

    protected void cleanupConnectionTemporaryDestinations() {

        for (TemporaryQueue tempQueue : connTempQueues) {
            try {
                tempQueue.delete();
            } catch (JMSException ex) {
                LOG.info("failed to delete Temporary Queue \"" + tempQueue.toString() + "\" on closing pooled connection: " + ex.getMessage());
            }
        }
        connTempQueues.clear();

        for (TemporaryTopic tempTopic : connTempTopics) {
            try {
                tempTopic.delete();
            } catch (JMSException ex) {
                LOG.info("failed to delete Temporary Topic \"" + tempTopic.toString() + "\" on closing pooled connection: " + ex.getMessage());
            }
        }
        connTempTopics.clear();
    }

    protected void cleanupAllLoanedSessions() {

        for (PooledSession session : loanedSessions) {
            try {
                session.close();
            } catch (JMSException ex) {
                LOG.info("failed to close laoned Session \"" + session + "\" on closing pooled connection: " + ex.getMessage());
            }
        }
        loanedSessions.clear();
    }

    public int getNumSessions() {
        return this.pool.getNumSessions();
    }

    public int getNumActiveSessions() {
        return this.pool.getNumActiveSessions();
    }

    public int getNumtIdleSessions() {
        return this.pool.getNumIdleSessions();
    }
}
