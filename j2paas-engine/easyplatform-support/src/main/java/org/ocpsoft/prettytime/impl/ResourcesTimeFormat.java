/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ocpsoft.prettytime.impl;

import org.ocpsoft.prettytime.Duration;
import org.ocpsoft.prettytime.LocaleAware;
import org.ocpsoft.prettytime.TimeFormat;
import org.ocpsoft.prettytime.format.SimpleTimeFormat;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Represents a simple method of formatting a specific {@link Duration} of time
 *
 * @author lb3
 */
public class ResourcesTimeFormat extends SimpleTimeFormat implements TimeFormat, LocaleAware<ResourcesTimeFormat>
{
   private ResourceBundle bundle;
   private final ResourcesTimeUnit unit;
   private TimeFormat override;

   public ResourcesTimeFormat(ResourcesTimeUnit unit)
   {
      this.unit = unit;
   }

   @Override
   public ResourcesTimeFormat setLocale(Locale locale)
   {
      bundle = ResourceBundle.getBundle(unit.getResourceBundleName(), locale);

      if (bundle instanceof TimeFormatProvider)
      {
         TimeFormat format = ((TimeFormatProvider) bundle).getFormatFor(unit);
         if (format != null)
         {
            this.override = format;
         }
      }
      else
      {
         override = null;
      }

      if (override == null)
      {
         setPattern(bundle.getString(unit.getResourceKeyPrefix() + "Pattern"));
         setFuturePrefix(bundle.getString(unit.getResourceKeyPrefix() + "FuturePrefix"));
         setFutureSuffix(bundle.getString(unit.getResourceKeyPrefix() + "FutureSuffix"));
         setPastPrefix(bundle.getString(unit.getResourceKeyPrefix() + "PastPrefix"));
         setPastSuffix(bundle.getString(unit.getResourceKeyPrefix() + "PastSuffix"));

         setSingularName(bundle.getString(unit.getResourceKeyPrefix() + "SingularName"));
         setPluralName(bundle.getString(unit.getResourceKeyPrefix() + "PluralName"));

         try {
            setFuturePluralName(bundle.getString(unit.getResourceKeyPrefix() + "FuturePluralName"));
         }
         catch (Exception e) {}
         try {
            setFutureSingularName((bundle.getString(unit.getResourceKeyPrefix() + "FutureSingularName")));
         }
         catch (Exception e) {}
         try {
            setPastPluralName((bundle.getString(unit.getResourceKeyPrefix() + "PastPluralName")));
         }
         catch (Exception e) {}
         try {
            setPastSingularName((bundle.getString(unit.getResourceKeyPrefix() + "PastSingularName")));
         }
         catch (Exception e) {}

      }

      return this;
   }

   @Override
   public String decorate(Duration duration, String time)
   {
      return override == null ? super.decorate(duration, time) : override.decorate(duration, time);
   }

   @Override
   public String decorateUnrounded(Duration duration, String time)
   {
      return override == null ? super.decorateUnrounded(duration, time) : override.decorateUnrounded(duration, time);
   }

   @Override
   public String format(Duration duration)
   {
      return override == null ? super.format(duration) : override.format(duration);
   }

   @Override
   public String formatUnrounded(Duration duration)
   {
      return override == null ? super.formatUnrounded(duration) : override.formatUnrounded(duration);
   }
}