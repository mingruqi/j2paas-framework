/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ocpsoft.prettytime.i18n;

import java.util.ListResourceBundle;

/**
 * Resource for Indonesian language.
 * 
 * @author xsalefter (xsalefter[at]gmail.com)
 */
public class Resources_in extends ListResourceBundle
{

   private static final Object[][] OBJECTS = new Object[][] {
            { "CenturyPattern", "%n %u" },
            { "CenturyFuturePrefix", "" },
            { "CenturyFutureSuffix", " dari sekarang" },
            { "CenturyPastPrefix", "" },
            { "CenturyPastSuffix", " yang lalu" },
            { "CenturySingularName", "abad" },
            { "CenturyPluralName", "abad" },
            { "DayPattern", "%n %u" },
            { "DayFuturePrefix", "" },
            { "DayFutureSuffix", " dari sekarang" },
            { "DayPastPrefix", "" },
            { "DayPastSuffix", " yang lalu" },
            { "DaySingularName", "hari" },
            { "DayPluralName", "hari" },
            { "DecadePattern", "%n %u" },
            { "DecadeFuturePrefix", "" },
            { "DecadeFutureSuffix", " dari sekarang" },
            { "DecadePastPrefix", "" },
            { "DecadePastSuffix", " yang lalu" },
            { "DecadeSingularName", "dekade" },
            { "DecadePluralName", "dekade" },
            { "HourPattern", "%n %u" },
            { "HourFuturePrefix", "" },
            { "HourFutureSuffix", " dari sekarang" },
            { "HourPastPrefix", "" },
            { "HourPastSuffix", " yang lalu" },
            { "HourSingularName", "jam" },
            { "HourPluralName", "jam" },
            { "JustNowPattern", "%u" },
            { "JustNowFuturePrefix", "" },
            { "JustNowFutureSuffix", "dari sekarang" },
            { "JustNowPastPrefix", "yang lalu" },
            { "JustNowPastSuffix", "" },
            { "JustNowSingularName", "" },
            { "JustNowPluralName", "" },
            { "MillenniumPattern", "%n %u" },
            { "MillenniumFuturePrefix", "" },
            { "MillenniumFutureSuffix", " dari sekarang" },
            { "MillenniumPastPrefix", "" },
            { "MillenniumPastSuffix", " yang lalu" },
            { "MillenniumSingularName", "ribuan tahun" },
            { "MillenniumPluralName", "ribuan tahun" },
            { "MillisecondPattern", "%n %u" },
            { "MillisecondFuturePrefix", "" },
            { "MillisecondFutureSuffix", " dari sekarang" },
            { "MillisecondPastPrefix", "" },
            { "MillisecondPastSuffix", " yang lalu" },
            { "MillisecondSingularName", "mili detik" },
            { "MillisecondPluralName", "mili detik" },
            { "MinutePattern", "%n %u" },
            { "MinuteFuturePrefix", "" },
            { "MinuteFutureSuffix", " dari sekarang" },
            { "MinutePastPrefix", "" },
            { "MinutePastSuffix", " yang lalu" },
            { "MinuteSingularName", "menit" },
            { "MinutePluralName", "menit" },
            { "MonthPattern", "%n %u" },
            { "MonthFuturePrefix", "" },
            { "MonthFutureSuffix", " dari sekarang" },
            { "MonthPastPrefix", "" },
            { "MonthPastSuffix", " yang lalu" },
            { "MonthSingularName", "bulan" },
            { "MonthPluralName", "bulan" },
            { "SecondPattern", "%n %u" },
            { "SecondFuturePrefix", "" },
            { "SecondFutureSuffix", " dari sekarang" },
            { "SecondPastPrefix", "" },
            { "SecondPastSuffix", " yang lalu" },
            { "SecondSingularName", "detik" },
            { "SecondPluralName", "detik" },
            { "WeekPattern", "%n %u" },
            { "WeekFuturePrefix", "" },
            { "WeekFutureSuffix", " dari sekarang" },
            { "WeekPastPrefix", "" },
            { "WeekPastSuffix", " yang lalu" },
            { "WeekSingularName", "minggu" },
            { "WeekPluralName", "minggu" },
            { "YearPattern", "%n %u" },
            { "YearFuturePrefix", "" },
            { "YearFutureSuffix", " dari sekarang" },
            { "YearPastPrefix", "" },
            { "YearPastSuffix", " yang lalu" },
            { "YearSingularName", "tahun" },
            { "YearPluralName", "tahun" },
            { "AbstractTimeUnitPattern", "" },
            { "AbstractTimeUnitFuturePrefix", "" },
            { "AbstractTimeUnitFutureSuffix", "" },
            { "AbstractTimeUnitPastPrefix", "" },
            { "AbstractTimeUnitPastSuffix", "" },
            { "AbstractTimeUnitSingularName", "" },
            { "AbstractTimeUnitPluralName", "" }
   };

   @Override
   protected Object[][] getContents()
   {
      return OBJECTS;
   }

}
