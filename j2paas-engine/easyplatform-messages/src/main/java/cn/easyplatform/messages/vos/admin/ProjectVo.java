/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos.admin;

import cn.easyplatform.type.ModeType;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ProjectVo implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;
	
	private String name;
	
	private String desp;
	
	private String appContext;
	
	private String bizDb;
	
	private String entityTableName;
	
	private String languages;
	
	private String identityDb;
	
	private Map<String, String> properties;

	private String theme;

	private ModeType mode;

	private int state;

	private String publicKey;

	private Map<String, Object> runtimeInfo;

	private List<DeviceMapVo> devices;

	private List<PortletVo> portlets;

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	/**
	 * @param id
	 */
	public ProjectVo(String id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return
	 */
	public String getDesp() {
		return desp;
	}

	/**
	 * @param desp
	 */
	public void setDesp(String desp) {
		this.desp = desp;
	}

	/**
	 * @return the appContext
	 */
	public String getAppContext() {
		return appContext;
	}

	/**
	 * @param appContext the appContext to set
	 */
	public void setAppContext(String appContext) {
		this.appContext = appContext;
	}

	/**
	 * @return the bizDb
	 */
	public String getBizDb() {
		return bizDb;
	}

	/**
	 * @param bizDb the bizDb to set
	 */
	public void setBizDb(String bizDb) {
		this.bizDb = bizDb;
	}

	/**
	 * @return the entityTableName
	 */
	public String getEntityTableName() {
		return entityTableName;
	}

	/**
	 * @param entityTableName the entityTableName to set
	 */
	public void setEntityTableName(String entityTableName) {
		this.entityTableName = entityTableName;
	}

	/**
	 * @return the languages
	 */
	public String getLanguages() {
		return languages;
	}

	/**
	 * @param languages the languages to set
	 */
	public void setLanguages(String languages) {
		this.languages = languages;
	}

	/**
	 * @return the identityDb
	 */
	public String getIdentityDb() {
		return identityDb;
	}

	/**
	 * @param identityDb the identityDb to set
	 */
	public void setIdentityDb(String identityDb) {
		this.identityDb = identityDb;
	}

	/**
	 * @return the properties
	 */
	public Map<String, String> getProperties() {
		return properties;
	}

	/**
	 * @param properties the properties to set
	 */
	public void setProperties(Map<String, String> properties) {
		this.properties = properties;
	}

	public ModeType getMode() {
		return mode;
	}

	public void setMode(ModeType mode) {
		this.mode = mode;
	}

	/**
	 * @return the runtimeInfo
	 */
	public Map<String, Object> getRuntimeInfo() {
		return runtimeInfo;
	}

	/**
	 * @param runtimeInfo the runtimeInfo to set
	 */
	public void setRuntimeInfo(Map<String, Object> runtimeInfo) {
		this.runtimeInfo = runtimeInfo;
	}


	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

	public List<DeviceMapVo> getDevices() {
		return devices;
	}

	public void setDevices(List<DeviceMapVo> devices) {
		this.devices = devices;
	}

	public List<PortletVo> getPortlets() {
		return portlets;
	}

	public void setPortlets(List<PortletVo> portlets) {
		this.portlets = portlets;
	}
}
