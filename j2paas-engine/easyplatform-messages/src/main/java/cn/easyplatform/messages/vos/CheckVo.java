/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos;

import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class CheckVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 625718023285389591L;

	private String user;
	
	private String password;

	private String toField;
	
	private String[] roles;

	/**
	 * @param user
	 * @param password
	 * @param toField
	 * @param roles
	 */
	public CheckVo(String user, String password, String toField, String[] roles) {
		this.user = user;
		this.password = password;
		this.toField = toField;
		this.roles = roles;
	}

	public String getUser() {
		return user;
	}

	public String getPassword() {
		return password;
	}

	public String getToField() {
		return toField;
	}

	public String[] getRoles() {
		return roles;
	}
	
}
