/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos.datalist;

import java.io.Serializable;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ListBatchVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7681146013551286923L;

	private String id;

	private List<Object[]> keys;

	private String code;

	/**
	 * @param id
	 * @param keys
	 * @param code
	 */
	public ListBatchVo(String id, List<Object[]> keys, String code) {
		this.id = id;
		this.keys = keys;
		this.code = code;
	}

	public String getId() {
		return id;
	}

	public List<Object[]> getKeys() {
		return keys;
	}

	public String getCode() {
		return code;
	}

}
