/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task.zkex.simple;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.type.FieldVo;
import cn.easyplatform.web.ext.Widget;
import cn.easyplatform.web.ext.jm.Mind;
import cn.easyplatform.web.ext.jm.Node;
import cn.easyplatform.web.task.OperableHandler;
import cn.easyplatform.web.task.event.EventListenerHandler;
import cn.easyplatform.web.task.zkex.ListSupport;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Events;

import java.util.*;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class MindBuilder extends AbstractQueryBuilder<Mind> implements Widget {

    public MindBuilder(OperableHandler mainTaskHandler, Mind comp) {
        super(mainTaskHandler, comp);
    }

    public MindBuilder(ListSupport support, Mind comp, Component anchor) {
        super(support, comp, anchor);
    }

    @Override
    public Component build() {
        me.setAttribute("$proxy", this);
        if (me.isImmediate())
            load();
        if (!Strings.isBlank(me.getEvent()))
            me.addEventListener(Events.ON_SELECTION, new EventListenerHandler(Events.ON_SELECTION,
                    main, me.getEvent(), anchor));
        if (!Strings.isBlank(me.getMoveEvent()))
            me.addEventListener(Events.ON_MOVE, new EventListenerHandler(Events.ON_MOVE,
                    main, me.getMoveEvent(), anchor));
        if (!Strings.isBlank(me.getAddEvent()))
            me.addEventListener(Mind.ON_NODE_ADD, new EventListenerHandler(Mind.ON_NODE_ADD,
                    main, me.getAddEvent(), anchor));
        if (!Strings.isBlank(me.getRemoveEvent()))
            me.addEventListener(Mind.ON_NODE_REMOVE, new EventListenerHandler(Mind.ON_NODE_REMOVE,
                    main, me.getRemoveEvent(), anchor));
        if (!Strings.isBlank(me.getUpdateEvent()))
            me.addEventListener(Mind.ON_NODE_UPDATE, new EventListenerHandler(Mind.ON_NODE_UPDATE,
                    main, me.getUpdateEvent(), anchor));
        if (!Strings.isBlank(me.getOpenEvent()))
            me.addEventListener(Events.ON_OPEN, new EventListenerHandler(Events.ON_OPEN,
                    main, me.getOpenEvent(), anchor));
        if (!Strings.isBlank(me.getCloseEvent()))
            me.addEventListener(Events.ON_CLOSE, new EventListenerHandler(Events.ON_CLOSE,
                    main, me.getCloseEvent(), anchor));
        return me;
    }

    @Override
    protected void createModel(List<?> data) {
        if (data.isEmpty()) return;
        FieldVo[] first = (FieldVo[]) data.remove(0);
        Node root = me.root(first[0].getValue(), (String) first[2].getValue());
        Map<Object, Node> map = new HashMap<>();
        map.put(root.getId(), root);
        List<Node> pending = new ArrayList<Node>();
        for (Object row : data) {
            FieldVo[] fv = (FieldVo[]) row;
            Node node = new Node(fv[0].getValue(), (String) fv[2].getValue());
            Node parent = map.get(fv[1].getValue());
            if (parent != null)
                parent.appendChild(node);
            else {
                node.setParent(new Node(fv[1].getValue()));
                pending.add(node);
            }
            map.put(node.getId(), node);
        }
        Iterator<Node> itr = pending.iterator();
        while (itr.hasNext()) {
            Node node = itr.next();
            Node parent = map.get(node.getParent().getId());
            if (parent != null) {
                parent.appendChild(node);
                itr.remove();
            }
        }
    }

    @Override
    public void reload(Component widget) {
        load();
    }
}
