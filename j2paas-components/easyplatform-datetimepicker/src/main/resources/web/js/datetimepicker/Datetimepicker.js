datetimepicker.Datetimepicker = zk.$extends(zul.Widget, {
	_value:'',
	_placeholder:'',
	_readonly: null,
	_format:'',
	_locale:'zh-cn',
	_showClose:false,
	_showTodayButton:false,
	_showClear:false,

	$define: {
        value: function() {},
        placeholder: function() {},
        readonly: function() {},
		format: function() {},
        locale: function() {},
        showClose: function() {},
        showTodayButton: function() {},
        showClear: function() {}
	},

    domAttrs_: function () {
        var attr = this.$supers('domAttrs_', arguments);
        return attr;
    },

	bind_: function () {
		this.$supers(datetimepicker.Datetimepicker,'bind_', arguments);
		var wgt = this;
		setTimeout(function () {
			wgt._init();
        },50)
	},

	unbind_: function () {
		this.$supers(datetimepicker.Datetimepicker,'unbind_', arguments);
	},

	doClick_: function (evt) {
		this.$super('doClick_', evt, true);//the super doClick_ should be called
	},

	_init: function () {
		// $('#'+this.uuid).parent().parent().css("overflow","unset");
		var self = this;
        $(function() {
            var datetimepicker = $('#'+self.uuid+' .input-group').datetimepicker({
                format: self._format,
                locale: moment.locale(self._locale),
                showClose:self._showClose,
                showTodayButton:self._showTodayButton,
                showClear:self._showClear
            });
            datetimepicker.on('dp.change', function(e) {
                self._value = $(e.target).find('input')[0].value;
                self.fire('onSelect', {datetime: self._value});
            });

        });
    }
});