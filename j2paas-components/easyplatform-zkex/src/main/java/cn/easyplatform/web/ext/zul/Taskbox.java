/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.zul;

import cn.easyplatform.web.ext.EntityExt;
import cn.easyplatform.web.ext.ManagedExt;
import cn.easyplatform.web.ext.Reloadable;
import cn.easyplatform.web.ext.Widget;
import org.zkoss.zul.Div;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Taskbox extends Div implements EntityExt, Reloadable, ManagedExt {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String _entity;

    private String _processCode;

    private boolean _immediate = true;

    private String _from;

    /**
     * 是否必需重新加载
     */
    private boolean _force;

    @Override
    public boolean isForce() {
        return _force;
    }

    public void setForce(boolean force) {
        this._force = force;
    }

    /**
     * @return the entity
     */
    public String getEntity() {
        return _entity;
    }

    /**
     * @param entity the entity to set
     */
    public void setEntity(String entity) {
        this._entity = entity;
    }

    /**
     * @return the processCode
     */
    public String getProcessCode() {
        return _processCode;
    }

    /**
     * @param processCode the processCode to set
     */
    public void setProcessCode(String processCode) {
        this._processCode = processCode;
    }

    /**
     * @return the immediate
     */
    public boolean isImmediate() {
        return _immediate;
    }

    /**
     * @param immediate the immediate to set
     */
    public void setImmediate(boolean immediate) {
        this._immediate = immediate;
    }

    /**
     * @return the from
     */
    public String getFrom() {
        return _from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(String from) {
        this._from = from;
    }

    /**
     *
     */
    public void reload() {
        Widget ext = (Widget) getAttribute("$proxy");
        ext.reload(this);
    }
}
