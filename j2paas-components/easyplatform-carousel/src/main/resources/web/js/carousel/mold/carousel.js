function(out) {

    out.push('<div ', this.domAttrs_(), ' class="auth" >');
    if (this._img !== null && this._img.length !== 0) {

            out.push('<div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">');
            out.push('<ol class="carousel-indicators">');
            for (var i = 0; i < this._img.length; i++) {
                if (i === 0)
                    out.push('<li data-target="#carouselExampleCaptions" data-slide-to="', i, '" class="active"/>');
                else
                    out.push('<li data-target="#carouselExampleCaptions" data-slide-to="', i, '"/>');
            }
            out.push('</ol>');
            out.push('<div class="carousel-inner">');
            for (var i = 0; i < this._img.length; i++) {
                if (i === 0) {
                    if (this._intervalTime != null &&　this._intervalTime.length !==0) {
                        if (this._imgevt !== null && this._imgevt.length > i ) {
                            out.push('<div class="carousel-item active"  data-interval="', this._intervalTime, '" onclick="zk.$(\'', this.uuid, '\').doLink(\'', this._imgevt[i], '\')">');
                        } else if (this._imgurl !== null && this._imgurl.length > i ) {
                            out.push('<div class="carousel-item active"  data-interval="', this._intervalTime, '" onclick="', this._imgurl[i],'">');
                        }else{
                            out.push('<div class="carousel-item active"  data-interval="', this._intervalTime, '" >');
                        }
                    } else {
                        if (this._imgevt !== null && this._imgevt.length > i ) {
                            out.push('<div class="carousel-item active"  data-interval="3000" onclick="zk.$(\'', this.uuid, '\').doLink(\'', this._imgevt[i], '\')">');
                        } else if (this._imgurl !== null && this._imgurl.length > i ) {
                            out.push('<div class="carousel-item active"  data-interval="3000" onclick="', this._imgurl[i],'">');
                        }else{
                            out.push('<div class="carousel-item active"  data-interval="3000" >');
                        }
                    }
                }else {
                    if (this._intervalTime != null &&　this._intervalTime.length !==0) {
                        if (this._imgevt !== null && this._imgevt.length !== 0 && this._imgevt.length > i ) {
                            out.push('<div class="carousel-item"  data-interval="', this._intervalTime, '" onclick="zk.$(\'', this.uuid, '\').doLink(\'', this._imgevt[i], '\')">');
                        } else if (this._imgurl !== null && this._imgurl.length !== 0 && this._imgurl.length > i ) {
                            out.push('<div class="carousel-item"  data-interval="', this._intervalTime, '" onclick="', this._imgurl[i],'">');
                        }else{
                            out.push('<div class="carousel-item"  data-interval="', this._intervalTime, '" >');
                        }
                    } else {
                        if (this._imgevt !== null && this._imgevt.length !== 0 && this._imgevt.length > i ) {
                            out.push('<div class="carousel-item"  data-interval="3000" onclick="zk.$(\'', this.uuid, '\').doLink(\'', this._imgevt[i], '\')">');
                        } else if (this._imgurl !== null && this._imgurl.length !== 0 && this._imgurl.length > i ) {
                            out.push('<div class="carousel-item"  data-interval="3000" onclick="', this._imgurl[i],'">');
                        }else{
                            out.push('<div class="carousel-item"  data-interval="3000" >');
                        }
                    }
                }
                if(this._query !== null && this._query.length !== 0) {
                    out.push('<img src="/servlets/download?id=', this._img[i], '" class="d-block w-100" />');
                }else{
                    out.push('<img src="', this._img[i], '" class="d-block w-100" />');
                }
                out.push('<div class="carousel-caption  d-md-block">');
                if (this._title !== null && this._title.length !== 0 && this._title.length>=i )
                    out.push('<h5>', this._title[i] ,'</h5>');
                if (this._content !== null && this._content !== 0 && this._content.length>=i )
                    out.push('<p>', this._content[i] ,'</p>');
                out.push('</div>');
                out.push('</div>');

            }
            out.push('</div>');
            out.push('<a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">');
            out.push('<span class="carousel-control-prev-icon" aria-hidden="true"></span>');
            out.push('</a>');
            out.push('<a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">');
            out.push('<span class="carousel-control-next-icon" aria-hidden="true"></span>');
            out.push('</a>');
            out.push('</div>');
    }else{
        out.push('<div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">\n' +
            '<ol class="carousel-indicators">\n' +
            '<li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>\n' +
            '<li data-target="#carouselExampleCaptions" data-slide-to="1"></li>\n' +
            '<li data-target="#carouselExampleCaptions" data-slide-to="2"></li>\n' +
            '</ol>\n' +
            '<div class="carousel-inner">\n' +
            '<div class="carousel-item active">\n' +
            '<img src="template/public/img/testimg.jpg" class="d-block w-100" alt="...">\n' +
            '<div class="carousel-caption  d-md-block">\n' +
            '<h5>预览效果</h5>\n' +
            '<p>未设置图片属性或找不到图片</p>\n' +
            '</div>\n' +
            '</div>\n' +
            '<div class="carousel-item">\n' +
            '<img src="template/public/img/testimg-2.jpg" class="d-block w-100" alt="...">\n' +
            '<div class="carousel-caption  d-md-block">\n' +
            '<h5>预览效果</h5>\n' +
            '<p>未设置图片属性或找不到图片</p>\n' +
            '</div>\n' +
            '</div>\n' +
            '<div class="carousel-item">\n' +
            '<img src="template/public/img/testimg-3.jpg" class="d-block w-100" alt="...">\n' +
            '<div class="carousel-caption  d-md-block">\n' +
            '<h5>预览效果</h5>\n' +
            '<p>未设置图片属性或找不到图片</p>\n' +
            '</div>\n' +
            '</div>\n' +
            '</div>\n' +
            '<a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">\n' +
            '<span class="carousel-control-prev-icon" aria-hidden="true"></span>\n' +
            '<span class="sr-only">Previous</span>\n' +
            '</a>\n' +
            '<a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">\n' +
            '<span class="carousel-control-next-icon" aria-hidden="true"></span>\n' +
            '<span class="sr-only">Next</span>\n' +
            ' </a>\n' +
            ' </div>');
    }
    out.push('</div>');
}