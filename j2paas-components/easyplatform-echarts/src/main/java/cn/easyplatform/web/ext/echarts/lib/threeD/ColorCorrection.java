/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.threeD;

import java.io.Serializable;

public class ColorCorrection implements Serializable {
    /**
     * 是否开启颜色纠正。
     */
    private boolean enable;
    /**
     * 颜色查找表，推荐使用。
     */
    private String lookupTexture;
    /**
     * 画面的曝光。
     */
    private int exposure;
    /**
     * 画面的亮度。
     */
    private int brightness;
    /**
     * 画面的对比度。
     */
    private int contrast;
    /**
     * 画面的饱和度。
     */
    private int saturation;

    public Boolean enable() {
        return this.enable;
    }
    public ColorCorrection enable(Boolean enable) {
        this.enable = enable;
        return this;
    }

    public String lookupTexture() {
        return this.lookupTexture;
    }
    public ColorCorrection lookupTexture(String lookupTexture) {
        this.lookupTexture = lookupTexture;
        return this;
    }

    public Integer exposure() {
        return this.exposure;
    }
    public ColorCorrection exposure(Integer exposure) {
        this.exposure = exposure;
        return this;
    }

    public Integer brightness() {
        return this.brightness;
    }
    public ColorCorrection brightness(Integer brightness) {
        this.brightness = brightness;
        return this;
    }

    public Integer contrast() {
        return this.contrast;
    }
    public ColorCorrection contrast(Integer contrast) {
        this.contrast = contrast;
        return this;
    }

    public Integer saturation() {
        return this.saturation;
    }
    public ColorCorrection saturation(Integer saturation) {
        this.saturation = saturation;
        return this;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public String getLookupTexture() {
        return lookupTexture;
    }

    public void setLookupTexture(String lookupTexture) {
        this.lookupTexture = lookupTexture;
    }

    public int getExposure() {
        return exposure;
    }

    public void setExposure(int exposure) {
        this.exposure = exposure;
    }

    public int getBrightness() {
        return brightness;
    }

    public void setBrightness(int brightness) {
        this.brightness = brightness;
    }

    public int getContrast() {
        return contrast;
    }

    public void setContrast(int contrast) {
        this.contrast = contrast;
    }

    public int getSaturation() {
        return saturation;
    }

    public void setSaturation(int saturation) {
        this.saturation = saturation;
    }
}
