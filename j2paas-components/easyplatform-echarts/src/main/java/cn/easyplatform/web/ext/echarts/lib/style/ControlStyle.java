/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.style;

/**
 * 时间轴控制器样式
 *
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ControlStyle {
    /**
     * 是否显示『控制按钮』。设置为 false 则全不显示
     */
    private Boolean show;
    /**
     * 是否显示『播放按钮
     */
    private Boolean showPlayBtn;
    /**
     * 是否显示『后退按钮』
     */
    private Boolean showPrevBtn;
    /**
     * 是否显示『前进按钮』
     */
    private Boolean showNextBtn;
    /**
     * 按钮大小
     */
    private Integer itemSize;
    /**
     * 按钮间隔
     */
    private Integer itemGap;
    /**
     * 播放按钮』的『可播放状态』的图形
     */
    private String playIcon;
    /**
     * 『播放按钮』的『可停止状态』的图形
     */
    private String stopIcon;
    /**
     * 按钮间隔
     */
    private String prevIcon;
    /**
     * 『前进按钮』的图形
     */
    private String nextIcon;

    private String position;

    private String color;

    private String borderColor;

    private Integer borderWidth;

    public String color() {
        return this.color;
    }

    public ControlStyle color(String color) {
        this.color = color;
        return this;
    }

    public String position() {
        return this.position;
    }

    public ControlStyle position(String position) {
        this.position = position;
        return this;
    }

    public String borderColor() {
        return this.borderColor;
    }

    public ControlStyle borderColor(String borderColor) {
        this.borderColor = borderColor;
        return this;
    }

    public Integer borderWidth() {
        return this.borderWidth;
    }

    public ControlStyle borderWidth(Integer borderWidth) {
        this.borderWidth = borderWidth;
        return this;
    }

    /**
     * 构造函数
     */
    public ControlStyle() {
    }

    public Boolean show() {
        return this.show;
    }

    public ControlStyle show(Boolean show) {
        this.show = show;
        return this;
    }

    public Boolean showPlayBtn() {
        return this.showPlayBtn;
    }

    public ControlStyle showPlayBtn(Boolean showPlayBtn) {
        this.showPlayBtn = showPlayBtn;
        return this;
    }

    public Boolean showPrevBtn() {
        return this.showPrevBtn;
    }

    public ControlStyle showPrevBtn(Boolean showPrevBtn) {
        this.showPrevBtn = showPrevBtn;
        return this;
    }

    public Boolean showNextBtn() {
        return this.showNextBtn;
    }

    public ControlStyle showNextBtn(Boolean showNextBtn) {
        this.showNextBtn = showNextBtn;
        return this;
    }

    public String prevIcon() {
        return this.prevIcon;
    }

    public ControlStyle prevIcon(String prevIcon) {
        this.prevIcon = prevIcon;
        return this;
    }

    public String stopIcon() {
        return this.stopIcon;
    }

    public ControlStyle stopIcon(String stopIcon) {
        this.stopIcon = stopIcon;
        return this;
    }

    public String nextIcon() {
        return this.nextIcon;
    }

    public ControlStyle nextIcon(String nextIcon) {
        this.nextIcon = nextIcon;
        return this;
    }

    public String playIcon() {
        return this.playIcon;
    }

    public ControlStyle playIcon(String playIcon) {
        this.playIcon = playIcon;
        return this;
    }

    /**
     * 获取itemSize值
     */
    public Integer itemSize() {
        return this.itemSize;
    }

    /**
     * 设置itemSize值
     *
     * @param itemSize
     */
    public ControlStyle itemSize(Integer itemSize) {
        this.itemSize = itemSize;
        return this;
    }

    /**
     * 获取itemGap值
     */
    public Integer itemGap() {
        return this.itemGap;
    }

    /**
     * 设置itemGap值
     *
     * @param itemGap
     */
    public ControlStyle itemGap(Integer itemGap) {
        this.itemGap = itemGap;
        return this;
    }

    /**
     * 获取itemSize值
     */
    public Integer getItemSize() {
        return itemSize;
    }

    /**
     * 设置itemSize值
     *
     * @param itemSize
     */
    public void setItemSize(Integer itemSize) {
        this.itemSize = itemSize;
    }

    /**
     * 获取itemGap值
     */
    public Integer getItemGap() {
        return itemGap;
    }

    /**
     * 设置itemGap值
     *
     * @param itemGap
     */
    public void setItemGap(Integer itemGap) {
        this.itemGap = itemGap;
    }

    public Boolean getShow() {
        return show;
    }

    public void setShow(Boolean show) {
        this.show = show;
    }

    public Boolean getShowPlayBtn() {
        return showPlayBtn;
    }

    public void setShowPlayBtn(Boolean showPlayBtn) {
        this.showPlayBtn = showPlayBtn;
    }

    public Boolean getShowPrevBtn() {
        return showPrevBtn;
    }

    public void setShowPrevBtn(Boolean showPrevBtn) {
        this.showPrevBtn = showPrevBtn;
    }

    public Boolean getShowNextBtn() {
        return showNextBtn;
    }

    public void setShowNextBtn(Boolean showNextBtn) {
        this.showNextBtn = showNextBtn;
    }

    public String getPlayIcon() {
        return playIcon;
    }

    public void setPlayIcon(String playIcon) {
        this.playIcon = playIcon;
    }

    public String getStopIcon() {
        return stopIcon;
    }

    public void setStopIcon(String stopIcon) {
        this.stopIcon = stopIcon;
    }

    public String getPrevIcon() {
        return prevIcon;
    }

    public void setPrevIcon(String prevIcon) {
        this.prevIcon = prevIcon;
    }

    public String getNextIcon() {
        return nextIcon;
    }

    public void setNextIcon(String nextIcon) {
        this.nextIcon = nextIcon;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(String borderColor) {
        this.borderColor = borderColor;
    }

    public Integer getBorderWidth() {
        return borderWidth;
    }

    public void setBorderWidth(Integer borderWidth) {
        this.borderWidth = borderWidth;
    }
}
