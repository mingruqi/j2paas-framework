/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.axis;

import cn.easyplatform.web.ext.echarts.lib.type.X;
import cn.easyplatform.web.ext.echarts.lib.type.Y;

/**
 * 坐标轴
 *
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Axis extends BaseAxis {

    /**
     * 是否显示
     */
    private Boolean show;
    /**
     * x 轴所在的 grid 的索引，默认位于第一个 grid
     */
    private Integer gridIndex;

    /**
     * 坐标轴类型，横轴默认为类目型'bottom'，纵轴默认为数值型'left'，可选为：'bottom' | 'top' | 'left' | 'right'
     */
    private Object position;


    public Boolean show() {
        return show;
    }

    public Axis show(Boolean show) {
        this.show = show;
        return this;
    }

    public Axis gridIndex(Integer gridIndex) {
        this.gridIndex = gridIndex;
        return this;
    }

    public Integer gridIndex() {
        return this.gridIndex;
    }

    /**
     * 获取position值
     */
    public Object position() {
        return this.position;
    }

    /**
     * 设置position值
     *
     * @param position
     */
    public Axis position(Object position) {
        this.position = position;
        return this;
    }

    /**
     * 设置position值
     *
     * @param position
     */
    public Axis position(X position) {
        this.position = position;
        return this;
    }

    /**
     * 设置position值
     *
     * @param position
     */
    public Axis position(Y position) {
        this.position = position;
        return this;
    }

    public Integer getGridIndex() {
        return gridIndex;
    }

    public void setGridIndex(Integer gridIndex) {
        this.gridIndex = gridIndex;
    }

    /**
     * 获取position值
     */
    public Object getPosition() {
        return position;
    }

    /**
     * 设置position值
     *
     * @param position
     */
    public void setPosition(Object position) {
        this.position = position;
    }

    public Boolean getShow() {
        return show;
    }

    public void setShow(Boolean show) {
        this.show = show;
    }
}
