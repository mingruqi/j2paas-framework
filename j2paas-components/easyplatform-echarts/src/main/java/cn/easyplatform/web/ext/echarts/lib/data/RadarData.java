/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.data;

import cn.easyplatform.web.ext.echarts.lib.style.AreaStyle;
import cn.easyplatform.web.ext.echarts.lib.style.ItemStyle;
import cn.easyplatform.web.ext.echarts.lib.style.LabelStyle;
import cn.easyplatform.web.ext.echarts.lib.style.LineStyle;

import java.io.Serializable;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class RadarData implements Serializable {

    private static final long serialVersionUID = 7218201600361155091L;

    /**
     * 名称
     */
    private String name;
    private Object value;
    private String symbol;
    private Object symbolSize;
    private Double symbolRotate;
    private Object symbolOffset;
    private LabelStyle label;
    private ItemStyle itemStyle;
    private LineStyle lineStyle;
    private AreaStyle areaStyle;

    public RadarData(String name, Object value) {
        this.name = name;
        this.value(value);
    }

    public String name() {
        return this.name;
    }

    public RadarData name(String name) {
        this.name = name;
        return this;
    }

    public Object value() {
        return this.value;
    }

    public RadarData value(Object value) {
        if (value instanceof String) {
            this.value = value.toString().split(";");
        } else
            this.value = value;
        return this;
    }

    public String symbol() {
        return this.symbol;
    }

    public RadarData symbol(String symbol) {
        this.symbol = symbol;
        return this;
    }

    public Object symbolSize() {
        return this.symbolSize;
    }

    public RadarData symbolSize(Object symbolSize) {
        this.symbolSize = symbolSize;
        return this;
    }

    public Double symbolRotate() {
        return this.symbolRotate;
    }

    public RadarData symbolRotate(Double symbolRotate) {
        this.symbolRotate = symbolRotate;
        return this;
    }

    public Object symbolOffset() {
        return this.symbolOffset;
    }

    public RadarData symbolOffset(Object symbolOffset) {
        this.symbolOffset = symbolOffset;
        return this;
    }

    public LabelStyle label() {
        if (label == null)
            label = new LabelStyle();
        return this.label;
    }

    public RadarData label(LabelStyle label) {
        this.label = label;
        return this;
    }

    public ItemStyle itemStyle() {
        if (itemStyle == null)
            itemStyle = new ItemStyle();
        return this.itemStyle;
    }

    public RadarData itemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
        return this;
    }

    public LineStyle lineStyle() {
        if (lineStyle == null)
            lineStyle = new LineStyle();
        return this.lineStyle;
    }

    public RadarData lineStyle(LineStyle lineStyle) {
        this.lineStyle = lineStyle;
        return this;
    }

    public AreaStyle areaStyle() {
        if (areaStyle == null)
            areaStyle = new AreaStyle();
        return this.areaStyle;
    }

    public RadarData areaStyle(AreaStyle areaStyle) {
        this.areaStyle = areaStyle;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Object getSymbolSize() {
        return symbolSize;
    }

    public void setSymbolSize(Object symbolSize) {
        this.symbolSize = symbolSize;
    }

    public Double getSymbolRotate() {
        return symbolRotate;
    }

    public void setSymbolRotate(Double symbolRotate) {
        this.symbolRotate = symbolRotate;
    }

    public Object getSymbolOffset() {
        return symbolOffset;
    }

    public void setSymbolOffset(Object symbolOffset) {
        this.symbolOffset = symbolOffset;
    }

    public LabelStyle getLabel() {
        return label;
    }

    public void setLabel(LabelStyle label) {
        this.label = label;
    }

    public ItemStyle getItemStyle() {
        return itemStyle;
    }

    public void setItemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
    }

    public LineStyle getLineStyle() {
        return lineStyle;
    }

    public void setLineStyle(LineStyle lineStyle) {
        this.lineStyle = lineStyle;
    }

    public AreaStyle getAreaStyle() {
        return areaStyle;
    }

    public void setAreaStyle(AreaStyle areaStyle) {
        this.areaStyle = areaStyle;
    }
}
