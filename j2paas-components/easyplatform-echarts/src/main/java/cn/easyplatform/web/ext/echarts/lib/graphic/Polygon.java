/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.graphic;

import cn.easyplatform.web.ext.echarts.lib.graphic.style.GraphicStyle;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Polygon extends Graphic {

    private Shape shape;

    private GraphicStyle style;

    public Polygon() {
        this.type("polygon");
    }

    public Shape shape() {
        if (shape == null)
            shape = new Shape();
        return shape;
    }

    public Polygon shape(Shape shape) {
        this.shape = shape;
        return this;
    }

    public GraphicStyle style() {
        if (style == null)
            style = new GraphicStyle();
        return style;
    }

    public Polygon style(GraphicStyle style) {
        this.style = style;
        return this;
    }

    public static class Shape {
        /**
         * 点列表，用于定义形状，如 [[22, 44], [44, 55], [11, 44], ...]
         */
        private Object[] points;
        /**
         * 是否平滑曲线。
         如果为 number：表示贝塞尔 (bezier) 差值平滑，smooth 指定了平滑等级，范围 [0, 1]。
         如果为 'spline'：表示 Catmull-Rom spline 差值平滑。
         */
        private Object smooth;
        /**
         * 是否将平滑曲线约束在包围盒中。smooth 为 number（bezier）时生效
         */
        private Boolean smoothConstraint;

        public Object[] points() {
            return this.points;
        }

        public Shape points(Object[] points) {
            this.points = points;
            return this;
        }

        public Object smooth() {
            return this.smooth;
        }

        public Shape smooth(Object smooth) {
            this.smooth = smooth;
            return this;
        }

        public Boolean smoothConstraint() {
            return this.smoothConstraint;
        }

        public Shape smoothConstraint(Boolean smoothConstraint) {
            this.smoothConstraint = smoothConstraint;
            return this;
        }

        public Object[] getPoints() {
            return points;
        }

        public void setPoints(Object[] points) {
            this.points = points;
        }

        public Object getSmooth() {
            return smooth;
        }

        public void setSmooth(Object smooth) {
            this.smooth = smooth;
        }

        public Boolean getSmoothConstraint() {
            return smoothConstraint;
        }

        public void setSmoothConstraint(Boolean smoothConstraint) {
            this.smoothConstraint = smoothConstraint;
        }
    }
}
