/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.graphic;

import cn.easyplatform.web.ext.echarts.lib.support.Point;
import cn.easyplatform.web.ext.echarts.lib.type.ShapeType;

import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Graphic extends Point implements Serializable {
    /**
     * @see ShapeType
     */
    private String type;
    /**
     * id 用于在更新或删除图形元素时指定更新哪个图形元素，如果不需要用可以忽略
     */
    private String id;
    /**
     * 指定本次对该图形元素的操作行为
     */
    private String $action;
    /**
     * 决定此图形元素在定位时，对自身的包围盒计算方式
     */
    private String bounding;
    /**
     * 是否不响应鼠标以及触摸事件
     */
    private Boolean silent;
    /**
     * 节点是否可见
     */
    private Boolean invisible;
    /**
     * 鼠标悬浮时鼠标的样式是什么。同 CSS 的 cursor
     */
    private String cursor;
    /**
     * 图形元素是否可以被拖拽
     */
    private Boolean draggable;
    /**
     * 是否渐进式渲染。当图形元素过多时才使用
     */
    private Boolean progressive;
    /**
     * 图形元素可以进行标准的 2D transform
     */
    private Object[] position;
    /**
     * 图形元素可以进行标准的 2D transform，
     */
    private Object[] scale;
    /**
     * 图形元素可以进行标准的 2D transform
     */
    private Object[] origin;
    /**
     * 图形元素可以进行标准的 2D transform
     */
    private Object rotation;
    /**
     * 事件
     */
    private String onclick;
    private String onmouseover;
    private String onmouseout;
    private String onmousemove;
    private String onmousewheel;
    private String onmousedown;
    private String onmouseup;
    private String ondrag;
    private String ondragstart;
    private String ondragend;
    private String ondragenter;
    private String ondragleave;
    private String ondragover;
    private String ondrop;

    public Object rotation() {
        return rotation;
    }

    public Graphic rotation(Object rotation) {
        this.rotation = rotation;
        return this;
    }

    public Object[] origin() {
        return origin;
    }

    public Graphic origin(Object[] origin) {
        this.origin = origin;
        return this;
    }

    public Object[] scale() {
        return scale;
    }

    public Graphic scale(Object[] scale) {
        this.scale = scale;
        return this;
    }

    public Object[] position() {
        return position;
    }

    public Graphic position(Object[] position) {
        this.position = position;
        return this;
    }

    public String type() {
        return type;
    }

    public Graphic type(String type) {
        this.type = type;
        return this;
    }

    public String id() {
        return id;
    }

    public Graphic id(String id) {
        this.id = id;
        return this;
    }

    public String $action() {
        return $action;
    }

    public Graphic $action(String $action) {
        this.$action = $action;
        return this;
    }

    public String bounding() {
        return bounding;
    }

    public Graphic bounding(String bounding) {
        this.bounding = bounding;
        return this;
    }

    public Boolean silent() {
        return silent;
    }

    public Graphic silent(Boolean silent) {
        this.silent = silent;
        return this;
    }

    public Boolean invisible() {
        return invisible;
    }

    public Graphic invisible(Boolean invisible) {
        this.invisible = invisible;
        return this;
    }

    public Boolean draggable() {
        return draggable;
    }

    public Graphic draggable(Boolean draggable) {
        this.draggable = draggable;
        return this;
    }

    public String cursor() {
        return cursor;
    }

    public Graphic cursor(String cursor) {
        this.cursor = cursor;
        return this;
    }

    public Boolean progressive() {
        return progressive;
    }

    public Graphic progressive(Boolean progressive) {
        this.progressive = progressive;
        return this;
    }

    public String onclick() {
        return onclick;
    }

    public Graphic onclick(String onclick) {
        this.onclick = onclick;
        return this;
    }

    public String onmouseover() {
        return onmouseover;
    }

    public Graphic onmouseover(String onmouseover) {
        this.onmouseover = onmouseover;
        return this;
    }

    public String onmouseout() {
        return onmouseout;
    }

    public Graphic onmouseout(String onmouseout) {
        this.onmouseout = onmouseout;
        return this;
    }

    public String onmousemove() {
        return onmousemove;
    }

    public Graphic onmousemove(String onmousemove) {
        this.onmousemove = onmousemove;
        return this;
    }

    public String onmousewheel() {
        return onmousewheel;
    }

    public Graphic onmousewheel(String onmousewheel) {
        this.onmousewheel = onmousewheel;
        return this;
    }

    public String onmousedown() {
        return onmousedown;
    }

    public Graphic onmousedown(String onmousedown) {
        this.onmousedown = onmousedown;
        return this;
    }

    public String onmouseup() {
        return onmouseup;
    }

    public Graphic onmouseup(String onmouseup) {
        this.onmouseup = onmouseup;
        return this;
    }

    public String ondrag() {
        return ondrag;
    }

    public Graphic ondrag(String ondrag) {
        this.ondrag = ondrag;
        return this;
    }

    public String ondragstart() {
        return ondragstart;
    }

    public Graphic ondragstart(String ondragstart) {
        this.ondragstart = ondragstart;
        return this;
    }

    public String ondragend() {
        return ondragend;
    }

    public Graphic ondragend(String ondragend) {
        this.ondragend = ondragend;
        return this;
    }

    public String ondragenter() {
        return ondragenter;
    }

    public Graphic ondragenter(String ondragenter) {
        this.ondragenter = ondragenter;
        return this;
    }

    public String ondragleave() {
        return ondragleave;
    }

    public Graphic ondragleave(String ondragleave) {
        this.ondragleave = ondragleave;
        return this;
    }

    public String ondragover() {
        return ondragover;
    }

    public Graphic ondragover(String ondragover) {
        this.ondragover = ondragover;
        return this;
    }

    public String ondrop() {
        return ondrop;
    }

    public Graphic ondrop(String ondrop) {
        this.ondrop = ondrop;
        return this;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String get$action() {
        return $action;
    }

    public void set$action(String $action) {
        this.$action = $action;
    }

    public String getBounding() {
        return bounding;
    }

    public void setBounding(String bounding) {
        this.bounding = bounding;
    }

    public Boolean getSilent() {
        return silent;
    }

    public void setSilent(Boolean silent) {
        this.silent = silent;
    }

    public Boolean getInvisible() {
        return invisible;
    }

    public void setInvisible(Boolean invisible) {
        this.invisible = invisible;
    }

    public String getCursor() {
        return cursor;
    }

    public void setCursor(String cursor) {
        this.cursor = cursor;
    }

    public Boolean getDraggable() {
        return draggable;
    }

    public void setDraggable(Boolean draggable) {
        this.draggable = draggable;
    }

    public Boolean getProgressive() {
        return progressive;
    }

    public void setProgressive(Boolean progressive) {
        this.progressive = progressive;
    }

    public String getOnclick() {
        return onclick;
    }

    public void setOnclick(String onclick) {
        this.onclick = onclick;
    }

    public String getOnmouseover() {
        return onmouseover;
    }

    public void setOnmouseover(String onmouseover) {
        this.onmouseover = onmouseover;
    }

    public String getOnmouseout() {
        return onmouseout;
    }

    public void setOnmouseout(String onmouseout) {
        this.onmouseout = onmouseout;
    }

    public String getOnmousemove() {
        return onmousemove;
    }

    public void setOnmousemove(String onmousemove) {
        this.onmousemove = onmousemove;
    }

    public String getOnmousewheel() {
        return onmousewheel;
    }

    public void setOnmousewheel(String onmousewheel) {
        this.onmousewheel = onmousewheel;
    }

    public String getOnmousedown() {
        return onmousedown;
    }

    public void setOnmousedown(String onmousedown) {
        this.onmousedown = onmousedown;
    }

    public String getOnmouseup() {
        return onmouseup;
    }

    public void setOnmouseup(String onmouseup) {
        this.onmouseup = onmouseup;
    }

    public String getOndrag() {
        return ondrag;
    }

    public void setOndrag(String ondrag) {
        this.ondrag = ondrag;
    }

    public String getOndragstart() {
        return ondragstart;
    }

    public void setOndragstart(String ondragstart) {
        this.ondragstart = ondragstart;
    }

    public String getOndragend() {
        return ondragend;
    }

    public void setOndragend(String ondragend) {
        this.ondragend = ondragend;
    }

    public String getOndragenter() {
        return ondragenter;
    }

    public void setOndragenter(String ondragenter) {
        this.ondragenter = ondragenter;
    }

    public String getOndragleave() {
        return ondragleave;
    }

    public void setOndragleave(String ondragleave) {
        this.ondragleave = ondragleave;
    }

    public String getOndragover() {
        return ondragover;
    }

    public void setOndragover(String ondragover) {
        this.ondragover = ondragover;
    }

    public String getOndrop() {
        return ondrop;
    }

    public void setOndrop(String ondrop) {
        this.ondrop = ondrop;
    }

    public Object[] getPosition() {
        return position;
    }

    public void setPosition(Object[] position) {
        this.position = position;
    }

    public Object[] getScale() {
        return scale;
    }

    public void setScale(Object[] scale) {
        this.scale = scale;
    }

    public Object[] getOrigin() {
        return origin;
    }

    public void setOrigin(Object[] origin) {
        this.origin = origin;
    }

    public Object getRotation() {
        return rotation;
    }

    public void setRotation(Object rotation) {
        this.rotation = rotation;
    }
}
