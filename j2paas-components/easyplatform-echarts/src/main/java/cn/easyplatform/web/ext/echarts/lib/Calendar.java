/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib;

import cn.easyplatform.web.ext.echarts.lib.axis.SplitLine;
import cn.easyplatform.web.ext.echarts.lib.style.ItemStyle;
import cn.easyplatform.web.ext.echarts.lib.style.LabelStyle;
import cn.easyplatform.web.ext.echarts.lib.support.Point;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Calendar extends Point {

    private Object width;

    private Object height;
    /**
     * 必填，日历坐标的范围 支持多种格式
     */
    private Object range;
    /**
     * 日历每格框的大小，可设置单值 或数组 第一个元素是宽 第二个元素是高。 支持设置自适应：auto, 默认为高宽均为20
     */
    private Object cellSize;
    /**
     * 日历坐标的布局朝向。
     * 'horizontal'
     * 'vertical'
     */
    private String orient;
    /**
     * 设置日历坐标分隔线的样式
     */
    private SplitLine splitLine;
    /**
     * 设置日历格的样式
     */
    private ItemStyle itemStyle;
    /**
     * 设置日历坐标中 星期轴的样式
     */
    private DayLabel dayLabel;
    /**
     * 设置日历坐标中 月份轴的样式
     */
    private MonthLabel monthLabel;
    /**
     * 设置日历坐标中 年的样式
     */
    private LabelStyle yearLabel;
    /**
     * 图形是否不响应和触发鼠标事件，默认为 false，即响应和触发鼠标事件
     */
    private Boolean silent;

    public Object width() {
        return this.width;
    }

    public Calendar width(Object width) {
        this.width = width;
        return this;
    }

    public Object height() {
        return this.height;
    }

    public Calendar height(Object height) {
        this.height = height;
        return this;
    }

    public Object getWidth() {
        return width;
    }

    public void setWidth(Object width) {
        this.width = width;
    }

    public Object getHeight() {
        return height;
    }

    public void setHeight(Object height) {
        this.height = height;
    }

    public Object range() {
        return range;
    }

    public Calendar range(Object range) {
        this.range = range;
        return this;
    }

    public Object cellSize() {
        return cellSize;
    }

    public Calendar cellSize(Object cellSize) {
        if (cellSize instanceof String) {
            if (((String) cellSize).indexOf(",") > 0) {
                this.cellSize = ((String) cellSize).split(",");
            } else
                this.cellSize = cellSize;
        } else
            this.cellSize = cellSize;
        return this;
    }

    public String orient() {
        return orient;
    }

    public Calendar orient(String orient) {
        this.orient = orient;
        return this;
    }

    public SplitLine splitLine() {
        if (splitLine == null)
            splitLine = new SplitLine();
        return splitLine;
    }

    public Calendar splitLine(SplitLine splitLine) {
        this.splitLine = splitLine;
        return this;
    }

    public ItemStyle itemStyle() {
        if (itemStyle == null)
            itemStyle = new ItemStyle();
        return itemStyle;
    }

    public Calendar itemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
        return this;
    }

    public DayLabel dayLabel() {
        if (dayLabel == null)
            dayLabel = new DayLabel();
        return dayLabel;
    }

    public Calendar dayLabel(DayLabel dayLabel) {
        this.dayLabel = dayLabel;
        return this;
    }

    public MonthLabel monthLabel() {
        if (monthLabel == null)
            monthLabel = new MonthLabel();
        return monthLabel;
    }

    public Calendar monthLabel(MonthLabel monthLabel) {
        this.monthLabel = monthLabel;
        return this;
    }

    public LabelStyle yearLabel() {
        if (yearLabel == null)
            yearLabel = new LabelStyle();
        return yearLabel;
    }

    public Calendar yearLabel(LabelStyle yearLabel) {
        this.yearLabel = yearLabel;
        return this;
    }

    public Boolean silent() {
        return silent;
    }

    public Calendar silent(Boolean silent) {
        this.silent = silent;
        return this;
    }

    public Object getRange() {
        return range;
    }

    public void setRange(Object range) {
        this.range = range;
    }

    public Object getCellSize() {
        return cellSize;
    }

    public void setCellSize(Object cellSize) {
        this.cellSize = cellSize;
    }

    public String getOrient() {
        return orient;
    }

    public void setOrient(String orient) {
        this.orient = orient;
    }

    public SplitLine getSplitLine() {
        return splitLine;
    }

    public void setSplitLine(SplitLine splitLine) {
        this.splitLine = splitLine;
    }

    public ItemStyle getItemStyle() {
        return itemStyle;
    }

    public void setItemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
    }

    public DayLabel getDayLabel() {
        return dayLabel;
    }

    public void setDayLabel(DayLabel dayLabel) {
        this.dayLabel = dayLabel;
    }

    public MonthLabel getMonthLabel() {
        return monthLabel;
    }

    public void setMonthLabel(MonthLabel monthLabel) {
        this.monthLabel = monthLabel;
    }

    public LabelStyle getYearLabel() {
        return yearLabel;
    }

    public void setYearLabel(LabelStyle yearLabel) {
        this.yearLabel = yearLabel;
    }

    public Boolean getSilent() {
        return silent;
    }

    public void setSilent(Boolean silent) {
        this.silent = silent;
    }

    public static class DayLabel extends LabelStyle {
        private Integer firstDay;
        private Object nameMap;

        public Integer firstDay() {
            return firstDay;
        }

        public DayLabel firstDay(Integer firstDay) {
            this.firstDay = firstDay;
            return this;
        }

        public Object nameMap() {
            return nameMap;
        }

        public DayLabel nameMap(Object nameMap) {
            this.nameMap = nameMap;
            return this;
        }

        public Integer getFirstDay() {
            return firstDay;
        }

        public void setFirstDay(Integer firstDay) {
            this.firstDay = firstDay;
        }

        public Object getNameMap() {
            return nameMap;
        }

        public void setNameMap(Object nameMap) {
            this.nameMap = nameMap;
        }
    }

    public static class MonthLabel extends LabelStyle {
        private Object nameMap;

        public Object nameMap() {
            return nameMap;
        }

        public MonthLabel nameMap(Object nameMap) {
            this.nameMap = nameMap;
            return this;
        }

        public Object getNameMap() {
            return nameMap;
        }

        public void setNameMap(Object nameMap) {
            this.nameMap = nameMap;
        }
    }
}
