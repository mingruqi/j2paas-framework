/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.style;

import cn.easyplatform.web.ext.echarts.lib.type.LineType;

import java.io.Serializable;

/**
 * 区域填充样式
 *
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class HandleStyle implements Serializable {

    private static final long serialVersionUID = -6547716731700677234L;

    /**
     * 颜色
     */
    private Object color;
    /**
     * 图形的描边颜色。支持的格式同 color
     */
    private Object borderColor;
    /**
     * 描边线宽。为 0 时无描边
     */
    private Integer borderWidth;
    /**
     * 柱条的描边类型，默认为实线，支持 'solid', 'dashed', 'dotted'
     */
    private LineType borderType;
    /**
     * 折线主线(IE8+)有效，阴影色彩，支持rgba
     */
    private String shadowColor;
    /**
     * 默认值5，折线主线(IE8+)有效，阴影模糊度，大于0有效
     */
    private Integer shadowBlur;
    /**
     * 默认值3，折线主线(IE8+)有效，阴影横向偏移，正值往右，负值往左
     */
    private Integer shadowOffsetX;
    /**
     * 默认值3，折线主线(IE8+)有效，阴影纵向偏移，正值往下，负值往上
     */
    private Integer shadowOffsetY;
    /**
     * 图形透明度。支持从 0 到 1 的数字，为 0 时不绘制该图形
     */
    private Double opacity;

    private Object areaColor;

    public Object areaColor() {
        return this.areaColor;
    }

    public HandleStyle areaColor(Object areaColor) {
        this.areaColor = areaColor;
        return this;
    }

    public LineType borderType() {
        return this.borderType;
    }

    public HandleStyle borderType(LineType borderType) {
        this.borderType = borderType;
        return this;
    }

    public Integer borderWidth() {
        return this.borderWidth;
    }

    public HandleStyle borderWidth(Integer borderWidth) {
        this.borderWidth = borderWidth;
        return this;
    }

    public Object borderColor() {
        return this.borderColor;
    }

    public HandleStyle borderColor(Object borderColor) {
        this.borderColor = borderColor;
        return this;
    }

    public Double opacity() {
        return this.opacity;
    }

    public HandleStyle opacity(Double opacity) {
        this.opacity = opacity;
        return this;
    }

    /**
     * 获取shadowColor值
     */
    public String shadowColor() {
        return this.shadowColor;
    }

    /**
     * 设置shadowColor值
     *
     * @param shadowColor
     */
    public HandleStyle shadowColor(String shadowColor) {
        this.shadowColor = shadowColor;
        return this;
    }

    /**
     * 获取shadowBlur值
     */
    public Integer shadowBlur() {
        return this.shadowBlur;
    }

    /**
     * 设置shadowBlur值
     *
     * @param shadowBlur
     */
    public HandleStyle shadowBlur(Integer shadowBlur) {
        this.shadowBlur = shadowBlur;
        return this;
    }

    /**
     * 获取shadowOffsetX值
     */
    public Integer shadowOffsetX() {
        return this.shadowOffsetX;
    }

    /**
     * 设置shadowOffsetX值
     *
     * @param shadowOffsetX
     */
    public HandleStyle shadowOffsetX(Integer shadowOffsetX) {
        this.shadowOffsetX = shadowOffsetX;
        return this;
    }

    /**
     * 获取shadowOffsetY值
     */
    public Integer shadowOffsetY() {
        return this.shadowOffsetY;
    }

    /**
     * 设置shadowOffsetY值
     *
     * @param shadowOffsetY
     */
    public HandleStyle shadowOffsetY(Integer shadowOffsetY) {
        this.shadowOffsetY = shadowOffsetY;
        return this;
    }

    /**
     * 获取color值
     */
    public Object color() {
        return this.color;
    }

    /**
     * 设置color值
     *
     * @param color
     */
    public HandleStyle color(Object color) {
        this.color = color;
        return this;
    }

    /**
     * 获取color值
     */
    public Object getColor() {
        return color;
    }

    /**
     * 设置color值
     *
     * @param color
     */
    public void setColor(Object color) {
        this.color = color;
    }

    public String getShadowColor() {
        return shadowColor;
    }

    public void setShadowColor(String shadowColor) {
        this.shadowColor = shadowColor;
    }

    public Integer getShadowBlur() {
        return shadowBlur;
    }

    public void setShadowBlur(Integer shadowBlur) {
        this.shadowBlur = shadowBlur;
    }

    public Integer getShadowOffsetX() {
        return shadowOffsetX;
    }

    public void setShadowOffsetX(Integer shadowOffsetX) {
        this.shadowOffsetX = shadowOffsetX;
    }

    public Integer getShadowOffsetY() {
        return shadowOffsetY;
    }

    public void setShadowOffsetY(Integer shadowOffsetY) {
        this.shadowOffsetY = shadowOffsetY;
    }

    public Double getOpacity() {
        return opacity;
    }

    public void setOpacity(Double opacity) {
        this.opacity = opacity;
    }

    public Object getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(Object borderColor) {
        this.borderColor = borderColor;
    }

    public Integer getBorderWidth() {
        return borderWidth;
    }

    public void setBorderWidth(Integer borderWidth) {
        this.borderWidth = borderWidth;
    }

    public LineType getBorderType() {
        return borderType;
    }

    public void setBorderType(LineType borderType) {
        this.borderType = borderType;
    }

    public Object getAreaColor() {
        return areaColor;
    }

    public void setAreaColor(Object areaColor) {
        this.areaColor = areaColor;
    }
}
