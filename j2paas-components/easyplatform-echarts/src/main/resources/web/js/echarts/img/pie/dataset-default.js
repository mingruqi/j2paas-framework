{
    legend: {},
    tooltip: {},
    dataset: {
        source: 'data'
    },
    series: [{
        type: 'pie',
        radius: 60,
        center: ['25%', '30%']
        // No encode specified, by default, it is '2012'.
    }, {
        type: 'pie',
        radius: 60,
        center: ['75%', '30%'],
        encode: {
            itemName: 'product',
            value: '2013'
        }
    }, {
        type: 'pie',
        radius: 60,
        center: ['25%', '75%'],
        encode: {
            itemName: 'product',
            value: '2014'
        }
    }, {
        type: 'pie',
        radius: 60,
        center: ['75%', '75%'],
        encode: {
            itemName: 'product',
            value: '2015'
        }
    }]
}