function (out) {

    out.push('<div ', this.domAttrs_(), ' id="',this.uuid,'" class="auth"  style="white-space:pre-line;margin-bottom:55px;">');

    if (this._contenta !== null && this._contenta.length !== 0 && this._contenta[0] !== "none") {

        for (var i = 0; i < this._temp; i++) {

            if (this._msgDate !== null && this._msgDate.length !== 0 && this._msgDate.length > i ) {
                out.push('<div class="media-body d-flex justify-content-center mb-2 mt-3">');
                out.push('<div class="text-white rounded-pill pl-2 pr-2" style="font-size: 11px;background: #E5E5E5;">', this._msgDate[i], '</div>');
                out.push('</div>')
            }

            out.push('<div class="card m-2 shadow3" style="border:none">');

            if ((this._author !== null && this._author.length !== 0 && this._author.length > i ) ||
                (this._vardate !== null && this._vardate.length !== 0 && this._vardate.length > i ) ||
                (this._portrait !== null && this._portrait.length !== 0 && this._portrait.length > i )) {
                out.push('<div class="pl-2 pr-2 pt-2 pb-2 card-head-line">');
                out.push('<div class="d-flex flex-row">');

                if (this._portrait !== null && this._portrait.length !== 0 && this._portrait.length > i ) {
                    if (this._query !== null && this._query.length !== 0 && this._portraitflag === true) {
                        out.push('<div><img src="/servlets/download?id=', this._portrait[i], '" class="mr-2" style="width:40px;height:40px;border-radius:1.5rem"></div>');
                    } else {
                        out.push('<div><img src="', this._portrait[i], '" class="mr-2" style="width:40px;height:40px;border-radius:1.5rem"></div>');
                    }
                }
                out.push('<div class="d-flex flex-column line-height-15">');
                if (this._author !== null && this._author.length !== 0 && this._author.length > i ) {
                    out.push('<div class="font-s-13 font-c-black">', this._author[i], '</div>');
                }

                if (this._vardate !== null && this._vardate.length !== 0 && this._vardate.length > i ) {
                    out.push('<div class="font-s-13 font-c-gray2">', this._vardate[i], '</div>');
                }
                out.push('</div>');
                out.push('</div>');
                out.push('</div>');
            }
            
            if (this._img !== null && this._img.length !== 0 && this._img.length > i ) {
                if (this._query !== null && this._query.length !== 0) {
                    out.push('<div><img src="/servlets/download?id=', this._img[i], '" style="width:100%;"></div>');
                } else {
                    out.push('<div><img src="', this._img[i], '" style="width:100%;"></div>');
                }
            }


            if (this._contentevt !== null && this._contentevt.length !== 0 && this._contentevt.length > i ) {
                out.push('<div onclick="zk.$(\'',this.uuid,'\').doTouch(\'',this._contentevt[i],'\')">');
            }else if(this._contenturl !== null && this._contenturl.length !== 0 && this._contenturl.length > i ){
                out.push('<div onclick="\'',this._contenturl,'\'">');
            }


            if (this._header !== null && this._header.length !== 0 && this._header.length > i ) {
                out.push('<div class="font-s-14 d-flex justify-content-between">');
                out.push('<div class="d-flex flex-row align-items-center line-height-15 pl-0 pr-2 pt-2 pb-2 font-s-14 text-dark font-weight-bold" style="width: 78%!important;">');
                if(this._msgStatus !== null && this._msgStatus.length !== 0 && this._msgStatus.length > i && this._msgStatus[i] == 0){
                    out.push('<div class="rounded-circle bg-danger ml-2" style="width: 8px;height: 8px;"></div>');
                }
                if (this._iconstyle !== null && this._iconstyle === "dingwei") {
                    out.push('<div class="iconfont icon-map mr-2" style="font-size: 22px;"></div>');
                }
                out.push('<div class="d-inline-block  text-truncate">', this._header[i], '</div>');
                out.push('</div>');
                out.push('<div>');
                if (this._flagcontent !== null && this._flagcontent.length !== 0 && this._flagcontent.length > i ) {
                    if (this._flagcolour !== null && this._flagcolour.length !== 0 && this._flagcolour.length > i ) {
                        out.push('<div class="pl-3 pr-3 pl-1 pr-1  font-s-13 text-white" style="border-radius:0 4px 0 4px;background-color:', this._flagcolour[i], '">', this._flagcontent[i], '</div>');
                    } else {
                        out.push('<div class="pl-3 pr-3 pl-1 pr-1  font-s-13 bg-success text-white" style="border-radius:0 4px 0 4px;">', this._flagcontent[i], '</div>');
                    }
                }

                out.push('</div>');
                out.push('</div>');
            }


            out.push('<div class="pl-2 pr-2 pt-1 card-head-line">');

            if (this._title !== null && this._title.length !==0 ) {
                for (var j = 0; j < this._title.length; j++) {

                    out.push('<div class="d-flex flex-row font-s-14">');
                    if (j == 0) {
                        if (this._contenta[i] !== null && this._contenta[i] !== "null" && this._contenta[i] !== "") {
                            out.push('<div class="text-black-50 pb-2" style="width: 27%">', this._title[j], '：</div>');
                            out.push('<div class="text-body pb-2">', this._contenta[i], '</div>');
                        }
                    } else if (j == 1 && this._contentb !== null && this._contentb.length > i ) {
                        if (this._contentb[i] !== null && this._contentb[i] !== "null" && this._contentb[i] !== "" && this._contentb.length>i) {
                            out.push('<div class="text-black-50 pb-2" style="width: 27%">', this._title[j], '：</div>');
                            out.push('<div class="text-body pb-2">', this._contentb[i], '</div>');
                        }
                    } else if (j == 2 && this._contentc !== null && this._contentc.length > i ) {
                        if (this._contentc[i] !== null && this._contentc[i] !== "null" && this._contentc[i] !== "" && this._contentc.length>i) {
                            out.push('<div class="text-black-50 pb-2" style="width: 27%">', this._title[j], '：</div>');
                            out.push('<div class="text-body pb-2">', this._contentc[i], '</div>');
                        }
                    } else if (j == 3 && this._contentd !== null && this._contentd.length > i ) {
                        if (this._contentd[i] !== null && this._contentd[i] !== "null" && this._contentd[i] !== "" && this._contentd.length>i) {
                            out.push('<div class="text-black-50 pb-2" style="width: 27%">', this._title[j], '：</div>');
                            out.push('<div class="text-body pb-2">', this._contentd[i], '</div>');
                        }
                    } else if (j == 4 && this._contente !== null && this._contente.length > i ) {
                        if (this._contente[i] !== null && this._contente[i] !== "null" && this._contente[i] !== "" && this._contente.length>i) {
                            out.push('<div class="text-black-50 pb-2" style="width: 27%">', this._title[j], '：</div>');
                            out.push('<div class="text-body pb-2">', this._contente[i], '</div>');
                        }
                    }

                    out.push('</div>');

                }
            } else {
                out.push('<div class="d-flex flex-row pb-2 font-s-14">');
                out.push('<div class="text-body">', this._contenta[i], '</div>');
                out.push('</div>');
                if (this._contentb !== null && this._contentb[i] !== "null" && this._contentb[i] !== "" && this._contentb.length > i ) {
                    out.push('<div class="d-flex flex-row pb-2 font-s-14">');
                    out.push('<div class="text-body">', this._contentb[i], '</div>');
                    out.push('</div>');
                } else if (this._contentc !== null && this._contentc[i] !== "null" && this._contentc[i] !== "" && this._contentc.length > i ) {
                    out.push('<div class="d-flex flex-row pb-2 font-s-14">');
                    out.push('<div class="text-body">', this._contentc[i], '</div>');
                    out.push('</div>');
                } else if (this._contentd !== null && this._contentd[i] !== "null" && this._contentd[i] !== "" && this._contentd.length > i ) {
                    out.push('<div class="d-flex flex-row pb-2 font-s-14">');
                    out.push('<div class="text-body">', this._contentd[i], '</div>');
                    out.push('</div>');
                } else if (this._contente !== null && this._contente[i] !== "null" && this._contente[i] !== "" && this._contente.length > i ) {
                    out.push('<div class="d-flex flex-row pb-2 font-s-14">');
                    out.push('<div class="text-body">', this._contente[i], '</div>');
                    out.push('</div>');
                }
            }


            out.push('</div>');

            if (this._contentevt !== null && this._contentevt.length !== 0 && this._contentevt.length > i ) {
                out.push('</div>');
            }

            if (this._linkstyle !== null && this._linkstyle === "button") {
                if (this._centreevt !== null && this._centreevt.length !== 0 && this._centreevt.length > i ) {
                    out.push('<div class="pl-3 pr-3 pt-2 pb-2 line-height-15 font-s-14 font-c-gray2">');

                    if (this._centretext !== null && this._centretext.length !== 0 ) {
                        out.push('<div id="" class="bg-primary pt-2 pb-2 text-white text-center rounded-pill" onclick="zk.$(\'', this.uuid, '\').doCentreLink(\'', this._centreevt[i], '\')">', this._centretext, '</div>');
                    } else {
                        out.push('<div id="" class="bg-primary pt-2 pb-2 text-white text-center rounded-pill" onclick="zk.$(\'', this.uuid, '\').doCentreLink(\'', this._centreevt[i], '\')">确定</div>');
                    }
                    out.push('</div>');

                } else if (this._centreurl !== null && this._centreurl.length !== 0 && this._centreurl.length > i ) {
                    out.push('<div class="pl-3 pr-3 pt-2 pb-2 line-height-15 font-s-14 font-c-gray2">');

                    if (this._centretext !== null &&  this._centretext.length !== 0 ) {
                        out.push('<div id="" class="bg-primary pt-2 pb-2 text-white text-center rounded-pill" onclick="', this._centretext[i],'">', this._centreurl, '</div>');
                    } else {
                        out.push('<div id="" class="bg-primary pt-2 pb-2 text-white text-center rounded-pill" onclick="', this._centreurl[i],'">确定</div>');
                    }
                    out.push('</div>');
                }
            } else {

                if ((this._frontevt !== null && this._frontevt.length !==0 &&this._frontevt.length > i ) ||
                    (this._centreevt !== null && this._centreevt.length !==0 &&this._centreevt.length > i ) ||
                    (this._tailevt !== null && this._tailevt.length !==0 && this._tailevt.length > i )) {
                    out.push('<div class="pl-2 pr-2 pt-2 pb-2 d-flex media-body flex-row align-items-center">');

                    if (this._frontevt !== null && this._frontevt.length !==0 && this._frontevt.length > i ) {
                        if (this._fronttext !== null && this._fronttext.length !== 0) {
                            out.push('<a id="" class="media-body d-flex media-body flex-row justify-content-start font-s-13" href="javascript:void(0)" onclick="zk.$(\'', this.uuid, '\').doFrontLink(\'', this._frontevt[i], '\')">', this._fronttext, '</a>');
                        } else {
                            out.push('<a id="" class="media-body d-flex media-body flex-row justify-content-start font-s-13" href="javascript:void(0)" onclick="zk.$(\'', this.uuid, '\').doFrontLink(\'', this._frontevt[i], '\')">thumb up</a>');
                        }
                    } else {
                        out.push('<a class="media-body d-flex media-body flex-row justify-content-start font-s-13"></a>')
                    }

                    if (this._centreevt !== null && this._centreevt.length !==0 && this._centreevt.length > i ) {
                        if (this._centretext !== null && this._centretext.length !== 0) {
                            out.push('<a id="" class="media-body d-flex media-body flex-row justify-content-center font-s-13" href="javascript:void(0)" onclick="zk.$(\'', this.uuid, '\').doCentreLink(\'', this._centreevt[i], '\')">', this._centretext, '</a>');
                        } else {
                            out.push('<a id="" class="media-body d-flex media-body flex-row justify-content-center font-s-13" href="javascript:void(0)" onclick="zk.$(\'', this.uuid, '\').doCentreLink(\'', this._centreevt[i], '\')">Comment</a>');
                        }
                    }

                    if (this._tailevt !== null && this._tailevt.length !==0 && this._tailevt.length > i ) {
                        if (this._tailtext !== null && this._tailtext.length !== 0) {
                            out.push('<a id="" class="media-body d-flex media-body flex-row justify-content-end font-s-13" href="javascript:void(0)" onclick="zk.$(\'', this.uuid, '\').doTailLink(\'', this._tailevt[i], '\')">', this._tailtext, '</a>');
                        } else {
                            out.push('<a id="" class="media-body d-flex media-body flex-row justify-content-end font-s-13" href="javascript:void(0)" onclick="zk.$(\'', this.uuid, '\').doTailLink(\'', this._tailevt[i], '\')">Link</a>');
                        }
                    } else {
                        out.push('<a class="media-body d-flex media-body flex-row justify-content-end font-s-13"></a>')
                    }

                    out.push('</div>');
                } else if ((this._fronturl !== null && this._fronturl.length !== 0 &&this._fronturl.length > i ) ||
                    (this._centreurl !== null && this._centreurl.length !== 0 && this._centreurl.length > i ) ||
                    (this._tailurl !== null && this._tailurl.length !== 0 && this._tailurl.length > i )) {
                    out.push('<div class="pl-2 pr-2 pt-2 pb-2 d-flex media-body flex-row align-items-center">');

                    if (this._fronturl !== null && this._fronturl.length !== 0 && this._fronturl.length > i ) {
                        if (this._fronttext !== null && this._fronttext.length !== 0) {
                            out.push('<a id="" href="', this._fronturl[i], '" class="media-body d-flex media-body flex-row justify-content-start font-s-13">', this._fronttext, '</a>');
                        } else {
                            out.push('<a id="" href="', this._fronturl[i], '" class="media-body d-flex media-body flex-row justify-content-start font-s-13">thumb up</a>');
                        }
                    } else {
                        out.push('<a class="media-body d-flex media-body flex-row justify-content-start font-s-13"></a>')
                    }

                    if (this._centreurl !== null && this._centreurl.length !== 0 && this._centreurl.length > i ) {
                        if (this._fronttext !== null && this._fronttext.length !== 0) {
                            out.push('<a id="" href="', this._centreurl[i], '" class="media-body d-flex media-body flex-row justify-content-center font-s-13">', this._centretext, '</a>');
                        } else {
                            out.push('<a id="" href="', this._centreurl[i], '" class="media-body d-flex media-body flex-row justify-content-center font-s-13">Comment</a>');
                        }
                    }

                    if (this._tailurl !== null && this._tailurl.length !== 0 && this._tailurl.length > i ) {
                        if (this._fronttext !== null && this._fronttext.length !== 0) {
                            out.push('<a id="" href="', this._tailurl[i], '" class="media-body d-flex media-body flex-row justify-content-end font-s-13">', this._tailtext , '</a>');
                        } else {
                            out.push('<a id="" href="', this._tailurl[i], '" class="media-body d-flex media-body flex-row justify-content-end font-s-13">Link</a>');
                        }
                    } else {
                        out.push('<a class="media-body d-flex media-body flex-row justify-content-end font-s-13"></a>')
                    }

                    out.push('</div>');
                }
            }
            out.push('</div>');
        }

        out.push('<div  class="text-muted font-s-14 pl-2">', this._moretxt, '</div>');

    } else if(this._contenta !== null && this._contenta.length !==0 && this._contenta[0] == "none"){
        out.push('<div style="display: flex;align-content: center;justify-content: center">');
        out.push('<img src="template/public/img/component-content-none.png"  style="width:160px;height:150px;"></div>');
        out.push('<div style="display: flex;align-content: center;justify-content: center">');
        out.push('<span style="color: #8a8a8a">未查询到数据</span>');
        out.push('</div>');
    } else {
        out.push('<div class="card ml-2 mr-2 mb-3 shadow3">\n' +
            '<div class="pl-2 pr-2 pt-2 pb-2 card-head-line">\n' +
            '<div class="d-flex flex-row">\n' +
            '<div><img src="template/public/img/testimg-3.jpg" class="mr-2 " style="width:40px;height:40px;border-radius:1.5rem"></div> \n' +
            '<div class="d-flex flex-column line-height-15">\n' +
            '<div class="font-s-13 font-c-black">吉鼎</div>\n' +
            '<div class="font-s-13 font-c-gray2">发表于 2020-05-09 15：30</div>\n' +
            '</div>\n' +
            '</div>\n' +
            '</div>\n' +
            '<img src="template/public/img/testimg.jpg" style="width:100%;" >\n' +

            '<div class="font-s-14 d-flex justify-content-between">\n' +
            '<div class="d-flex flex-row align-items-center line-height-15 pl-2 pt-1 font-s-14 text-dark font-weight-bold" style="width: 80%!important;">\n' +
            '<div class="iconfont icon-map mr-2" style="font-size: 22px;"></div>\n' +
            '<div class="d-inline-block  text-truncate">标题</div>\n' +
            '</div>\n' +
            '<div>\n' +
            '<div class="pl-3 pr-3 pl-1 pr-1  font-s-13 bg-success text-white" style="border-radius:0 4px 0 4px;">标签</div>\n' +
            '</div>\n' +
            '</div>\n' +

            '<div class="pl-2 pr-2 pt-1 card-head-line">\n' +
            '<h5 class="pt-1 font-s-14 font-c-gray2 line-height-15">卡片视图</h5>\n' +
            '<p class="font-s-14">正文，此为预览效果，未设置列表的显示内容或属性设置错误。</p>\n' +
            '</div>\n' +
            '<div class="pl-2 pr-2 pt-2 pb-2 d-flex media-body flex-row align-items-center">\n' +
            '<a href="#" class="media-body d-flex media-body flex-row justify-content-start font-s-13">thumb up</a>\n' +
            '<a href="#" class="media-body d-flex media-body flex-row justify-content-center font-s-13">Comment</a>\n' +
            '<a href="#" class="media-body d-flex media-body flex-row justify-content-end font-s-13">Read more</a>\n' +
            '</div>\n' +
            '</div>');
    }

    /*out.push('<script>$(window).scroll(function(){ \n' +
        'zk.$(\'', this.uuid, '\').scrollWindow()\n ' +
        '});</script>\n');*/

    out.push('</div>');
}
